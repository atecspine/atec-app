import React from 'react';
import {
  Text, 
  View, 
  TextInput, 
  Button,
  TouchableOpacity,
} from 'react-native';
import AppColors from '../common/AppColors';
import AppButton from '../common/AppButton';
import { createStylesheet } from '../common/AppStyleSheet';
import { Heading1, Paragraph } from '../common/AppText';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {
  loginWithEmail,
} from '../actions';

class LoginScreen extends React.Component {
  static propTypes = {
    user: PropTypes.object,
    isLoggedIn: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      email: this.props.user.email,
      password: '',
    }

    this.logIn = this.logIn.bind(this);
    this.handleForgotPassword = this.handleForgotPassword.bind(this);
  }

  logIn() {
    if (this.state.isLoading) {
      return;
    } else if (this.state.email === '') {
      alert('Email is required.');
    } else if (this.state.password === '') {
      alert('Password is required.');
    } else {
      this.setState({isLoading: true});
      this.props.logInWithEmail(this.state.email, this.state.password).then(() => {
        this.setState({isLoading: false});
      }).catch(err => {
        this.setState({isLoading: false});
        if (err.code === 'auth/user-disabled') {
          alert('This account has been disabled. Please contact support@atecspine.com if this was in error.');
        } else if (err.code === 'auth/user-not-found') {
          alert('There is no account with this email.');
        } else {
          alert('Invalid password. Please try again.');
        }
      });
    }
  }

  handleForgotPassword() {
    this.props.navigator.push({
      screen: 'atec.ForgotPasswordView',
      title: 'Forgot Password',
      titleImage: require('../common/img/header-logo.png'),
      backButtonTitle: '',
      passProps: {
        email: this.state.email,
      }
    });
  }

  render() {
    let loginButton;
    if (this.state.isLoading) {
      loginButton = (
        <AppButton
          caption="Logging in..."
          type="primary"
        />
      );
    } else {
      loginButton = (
        <AppButton
          caption="Login"
          onPress={() => this.logIn()}
          type="primary"
        />
      );
    }
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={styles.heading}>
            <Heading1>Login</Heading1>
          </View>
          <View style={styles.loginForm}>
            <View style={styles.textInputWrap}>
              <TextInput
                ref="emailField"
                autoFocus={true}
                placeholder="Email"
                autoCorrect={false}
                multiline={false}
                autoCapitalize="none"
                keyboardType="email-address"
                clearButtonMode="while-editing"
                returnKeyType="next"
                onSubmitEditing={() => this.refs.passwordField.focus()}
                onChangeText={(text) => this.setState({email: text})}
                style={styles.textField}
                value={this.state.email}
                underlineColorAndroid="transparent"
              />
            </View>
            <View style={styles.textInputWrap}>
              <TextInput
                ref="passwordField"
                autoFocus={false}
                placeholder="Password"
                multiline={false}
                autoCapitalize="none"
                keyboardType="default"
                returnKeyType="go"
                onSubmitEditing={() => this.logIn()}
                onChangeText={(text) => this.setState({password: text})}
                secureTextEntry={true}
                style={styles.textField}
                value={this.state.password}
                underlineColorAndroid="transparent"
              />
            </View>
            {loginButton}
          </View>
          <TouchableOpacity
              style={styles.forgotLink}
              accessibilityLabel="Forgot Password"
              accessibilityTraits="button"
              onPress={() => this.handleForgotPassword()}
            >
              <Paragraph style={styles.link}>Forgot Password?</Paragraph>
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const INPUT_HEIGHT = 45;
const styles = createStylesheet({
  container: {
    flex: 1,
    backgroundColor: AppColors.white,
    alignItems: 'center',
    borderColor: AppColors.brightGreen,
    borderTopWidth: 2,
  },
  content: {
    flex: 1,
    padding: 30,
    maxWidth: 500,
    width: '100%',
  },
  heading: {
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  loginForm: {
    marginVertical: 10,
  },
  textInputWrap: {
    position: 'relative',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: AppColors.brightGreen,
    marginVertical: 5,
  },
  textField: {
    fontFamily: 'Soleto',
    fontSize: 16,
    height: INPUT_HEIGHT,
    paddingHorizontal: 15,
    paddingVertical: 10,
    android: {
      textAlignVertical: 'top',
    },
  },
  forgotLink: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  link: {
    color: AppColors.lightText,
  }
});

function actions(dispatch) {
  return {
    logInWithEmail: (email, password) => dispatch(loginWithEmail(email, password)),
  };
}

function select(state) {
  return {
    isLoading: state.user.isLoading,
    isLoggedIn: state.user.isLoggedIn,
    user: state.user,
  };
}

module.exports = connect(select, actions)(LoginScreen);
