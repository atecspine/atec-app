//@flow
'use strict';

import React from 'React';
import {
  View,
  Platform,
  TextInput,
} from 'react-native';
import AppColors from '../common/AppColors';
import AppButton from '../common/AppButton';
import { createStylesheet } from '../common/AppStyleSheet';
import { Heading1, Paragraph } from '../common/AppText';
import { sendForgotPasswordEmail } from '../actions';
import { connect } from 'react-redux';

class ForgotPasswordView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      email: this.props.email,
    };

    this.forgotPassword = this.forgotPassword.bind(this);
    this.close = this.close.bind(this);
  }

  forgotPassword() {
    if (this.state.isLoading) {
      return;
    } else if (this.state.email === '') {
      alert('Email is required.');
    } else {
      this.setState({isLoading: true});
      this.props.sendForgotPasswordEmail(this.state.email).then((result) => {
        this.setState({isLoading: false});
        this.close();
        alert('Reset password email sent!');
      }).catch((error) => {
        this.setState({isLoading: false});
        alert(error);
      });
    }
  }

  close() {
    if (this.props.navigator) {
      this.props.navigator.pop();
    }
  }

  render() {
    let forgotPasswordButton;
    if (this.state.isLoading) {
      forgotPasswordButton = (
        <AppButton
          buttonStyle={styles.forgotPasswordButton}
          caption="Sending..."
          type="primary"
        />
      );
    } else {
      forgotPasswordButton = (
        <AppButton
          caption="Reset Password"
          buttonStyle={styles.forgotPasswordButton}
          onPress={() => this.forgotPassword()}
          type="primary"
        />
      );
    }

    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={styles.heading}>
            <Heading1 style={styles.headingText}>Forgot Password?</Heading1>
            <View style={styles.information}>
              <Paragraph style={{color: AppColors.lightText, textAlign: 'center'}}>
                We'll send instructions to reset your password.
              </Paragraph>
            </View>
          </View>
          <View style={styles.loginForm}>
            <View style={styles.textInputWrap}>
              <TextInput
                ref="emailField"
                autoFocus={true}
                placeholder="Email"
                autoCorrect={false}
                multiline={false}
                autoCapitalize="none"
                keyboardType="email-address"
                returnKeyType="done"
                clearButtonMode="while-editing"
                onSubmitEditing={() => this.forgotPassword()}
                onChangeText={(text) => this.setState({email: text})}
                style={styles.textField}
                value={this.state.email}
                underlineColorAndroid="transparent"
              />
            </View>
            {forgotPasswordButton}
          </View>
        </View>
      </View>
    );
  }
}

const INPUT_HEIGHT = 45;

const styles = createStylesheet({
  container: {
    flex: 1,
    backgroundColor: AppColors.white,
    alignItems: 'center',
    borderColor: AppColors.brightGreen,
    borderTopWidth: 2,
  },
  content: {
    flex: 1,
    padding: 30,
    maxWidth: 500,
    width: '100%',
  },
  heading: {
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  headingText: {
    color: AppColors.darkBlue,
  },
  information: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  link: {
    color: AppColors.inactiveText,
    marginLeft: 5,
    textDecorationLine: 'underline',
  },
  loginForm: {
    marginVertical: 10,
  },
  textInputWrap: {
    position: 'relative',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: AppColors.brightGreen,
    marginVertical: 5,
  },
  textField: {
    fontFamily: 'Soleto',
    fontSize: 16,
    height: INPUT_HEIGHT,
    paddingHorizontal: 15,
    paddingVertical: 10,
    android: {
      textAlignVertical: 'top',
    },
  },
  togglePasswordLink: {
    position: 'absolute',
    top: 0,
    right: 0,
    height: INPUT_HEIGHT,
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  togglePasswordLinkText: {
    fontFamily: 'Soleto',
    color: AppColors.inactiveText,
    fontSize: 16,
  },
  forgotLink: {
    justifyContent: 'center',
    alignItems: 'center',
  }
});

function actions(dispatch) {
  return {
    sendForgotPasswordEmail: (email) => dispatch(sendForgotPasswordEmail(email)),
  };
}

module.exports = connect(null, actions)(ForgotPasswordView);
