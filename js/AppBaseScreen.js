import React from 'react';
import { View } from 'react-native';
import DownloadingOverlay from './common/DownloadingOverlay';

export default class AppBaseScreeen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.renderContent()}
        <DownloadingOverlay />
      </View>
    );
  }
}
