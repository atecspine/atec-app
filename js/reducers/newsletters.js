
const initialState = {
  isLoading: false,
  list: [],
};

function newsletters(state = initialState, action) {
  if (action.type === 'NEWSLETTERS_UPDATED') {
    return {
      ...state,
      list: action.newsletters,
    }
  }
  return state;
}

module.exports = newsletters;