const initialState = {
  root: 'login'
};

function app(state = initialState, action) {
  switch (action.type) {
    case 'CHANGED_APP_ROOT':
      return {
        root: action.root
      };
    default:
      return state;
  }
}

module.exports = app;
