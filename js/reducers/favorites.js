
const initialState = {
  isLoading: false,
  list: [],
};

function favorites(state = initialState, action) {
  if (action.type === 'ADD_FAVORITE_FILE') {
    let favorites = state.list;
    favorites = favorites.concat({
      ...action.file,
      added_on: action.added_on
    });
    return {
      ...state,
      list: favorites,
    };
  }
  if (action.type === 'REMOVE_FAVORITE_FILE') {
    let favorites = state.list;
    favorites = favorites.filter((f) => f.file_url !== action.file.file_url);
    return {
      ...state,
      list: favorites,
    };
  }
  if (action.type === 'FILE_SAVED_FOR_OFFLINE') {
    let favorites = state.list.map((file) => {
      if (file.file_url === action.file.file_url) {
        return action.file;
      }
      return file;
    });
    return {...state, list: favorites};
  }

  return state;
}

module.exports = favorites;