
const initialState = {
  query: '',
  results: [],
};

function search(state = initialState, action) {
  if (action.type === 'SEARCH_QUERY_UPDATED') {
    return {
      ...state,
      query: action.query,
    }
  }
  return state;
}

module.exports = search;