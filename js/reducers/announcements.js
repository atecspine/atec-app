
const initialState = {
  isLoading: false,
  list: [],
};

function announcements(state = initialState, action) {
  if (action.type === 'ANNOUNCEMENTS_UPDATED') {
    return {
      ...state,
      list: action.announcements,
    }
  }
  return state;
}

module.exports = announcements;