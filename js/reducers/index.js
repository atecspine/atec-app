var { combineReducers } = require('redux');

module.exports = combineReducers({
  app: require('./app'),
  user: require('./user'),
  products: require('./products'),
  history: require('./history'),
  search: require('./search'),
  favorites: require('./favorites'),
  folders: require('./folders'),
  folderMap: require('./folderMap'),
  sharing: require('./sharing'),
  downloadingOverlay: require('./downloadingOverlay'),
  contact: require('./contact'),
  announcements: require('./announcements'),
  newsletters: require('./newsletters'),
});
