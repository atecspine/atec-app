
import _ from 'lodash';

const initialState = [];

function folders(state = initialState, action) {
  if (action.type === 'ADD_FOLDER') {
    return [action.folder, ...state];
  }
  if (action.type === 'REMOVE_FOLDERS') {
    const allFolders = state.filter(f => !_.includes(action.folders, f));
    return [
      ...allFolders
    ];
  }
  return state;
}

module.exports = folders;
