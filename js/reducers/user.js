
const initialState = {
  isLoggedIn: false,
  email: '',
};

function user(state = initialState, action = {}) {
  if (action.type === 'LOGGED_IN') {
    let { _user } = action.user.user; // quirky firebase return object
    return {
      ...state,
      isLoggedIn: true,
      ..._user,
    };
  }
  return state;
}

module.exports = user;
