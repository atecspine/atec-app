
const initialState = {
  list: [],
};

function history(state = initialState, action = {}) {
  if (action.type === 'FILE_ADDED_TO_HISTORY') {
    let history = state.list.concat(action.file);
    return {
      ...state,
      list: history,
    }
  }
  return state;
}

module.exports = history;
