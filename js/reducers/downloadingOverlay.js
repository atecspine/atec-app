const initialState = {
  visible: false,
  files: [],
};

function downloadingOverlay(state = initialState, action) {
  if (action.type === 'FILE_STARTED_DOWNLOADING') {
    return {
      ...state,
      files: state.files.concat({
        path: action.file,
        bytesTransferred: 0,
        totalBytes: 0,
      }),
      visible: true,
    }
  }
  if (action.type === 'FILE_FINISHED_DOWNLOADING') {
    const files = state.files.filter(f => f.path !== action.file);
    const visible = files.length > 0 ? true : false;
    return {
      ...state,
      files: files,
      visible: visible,
    }
  }
  if (action.type === 'UPDATE_FILE_DOWNLOAD_PROGRESS') {
    const files = state.files.map(f => {
      if (f.path === action.file) {
        return {
          path: action.file,
          bytesTransferred: action.bytesTransferred,
          totalBytes: action.totalBytes,
        }
      } 

      return f;
    });

    return {
      ...state,
      files
    }
  }
  if (action.type === 'HIDE_DOWNLOADING_OVERLAY') {
    return initialState;
  }
  return state
}

module.exports = downloadingOverlay;
