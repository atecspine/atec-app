
const initialState = {
  isLoading: false,
  list: [],
};

function products(state = initialState, action) {
  if (action.type === 'PRODUCTS_UPDATED') {
    return {
      ...state,
      list: action.products,
    }
  }
  return state;
}

module.exports = products;