import _ from 'lodash';

const initialState = [];

function folderMap(state = initialState, action) {
  if (action.type === 'ADD_FILES_TO_FOLDERS') {
    let additionalFiles = [];
    action.payload.files.forEach(file => {
      additionalFiles = [
        ...additionalFiles,
        ...action.payload.folders.map(folder => ({file, folder}))
      ];
    });
    const allFiles = [
      ...state,
      ...additionalFiles
    ];
    const filtered = allFiles.filter((item, index, self) => index === self.findIndex((t) => t.file === item.file && t.folder === item.folder))
    return filtered;
  }

  if (action.type === 'REMOVE_FILES_FROM_FOLDER') {
    return state.filter(item => !(_.includes(action.payload.files, item.file) && item.folder === action.payload.folder));
  }
  if (action.type === 'REMOVE_FOLDERS') {
    const updatedMap = state.filter(item => !_.includes(action.folders, item.folder));
    return [
      ...updatedMap
    ];
  }
  return state;
}

module.exports = folderMap;
