
const initialState = {
  selectedFiles: [],
  selectedFolders: []
};

function sharing(state = initialState, action) {
  if (action.type === 'ADD_FILE_TO_SHARING') {
    return {
      ...state,
      selectedFiles: state.selectedFiles.concat(action.file),
    };
  }
  if (action.type === 'REMOVE_FILE_FROM_SHARING') {
    let files = state.selectedFiles;
    return {
      ...state,
      selectedFiles: files.filter(f => f.file_url !== action.file.file_url),
    };
  }
  if (action.type === 'RESET_FILES_TO_SHARE') {
    return {
      ...state,
      selectedFiles: []
    };
  }
  if (action.type === 'ADD_FOLDER_TO_SHARING') {
    return {
      ...state,
      selectedFolders: state.selectedFolders.concat(action.folder),
    };
  }
  if (action.type === 'REMOVE_FOLDER_FROM_SHARING') {
    let folders = state.selectedFolders;
    return {
      ...state,
      selectedFolders: folders.filter(f => f !== action.folder)
    };
  }
  if (action.type === 'RESET_FOLDERS_TO_SHARE') {
    return {
      ...state,
      selectedFolders: []
    };
  }
  return state;
}

module.exports = sharing;