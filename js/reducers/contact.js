
const initialState = {
  isLoading: false,
  list: [],
};

function contact(state = initialState, action) {
  if (action.type === 'CONTACT_UPDATED') {
    return {
      ...state,
      list: action.contacts,
    }
  }
  return state;
}

module.exports = contact;