
import React, {Component} from 'React';
import {
  View,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import {createStylesheet} from './AppStyleSheet';
import AppColors from './AppColors';

class TextField extends Component {
  render() {
    let detail;
    if (this.props.isLoading) {
      detail = <ActivityIndicator />;
    }

    return (
      <View style={styles.wrap}>
        <TextInput
          ref={this.props.inputRef}
          style={styles.input}
          placeholder={this.props.placeholderText}
          placeholderTextColor={AppColors.lightText}
          value={this.props.value}
          onChangeText={this.props.onChange}
          editable={this.props.readOnly ? false : true}
          clearButtonMode="while-editing"
          keyboardType={this.props.keyboardType || 'default'}
          onBlur={this.props.onBlur}
          underlineColorAndroid={'transparent'}
          {...this.props}
        />
        {detail}
      </View>
    );
  }
}

const styles = createStylesheet({
  wrap: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  input: {
    fontFamily: 'Soleto',
    flex: 1,
    height: 29,
    backgroundColor: 'transparent',
    fontSize: 16,
    android: {
      padding: 0,
    },
  },
});

module.exports = TextField;
