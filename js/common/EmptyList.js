// @flow
'use strict';

import React, {Component} from 'React';
import {
  StyleSheet,
  View,
  Text,
  Image,
} from 'react-native';
import AppColors from './AppColors';

class EmptyList extends Component {
  render() {
    const image = this.props.image &&
      <Image style={styles.image} source={this.props.image} />;
    const title = this.props.title &&
      <Text style={styles.title}>{this.props.title}</Text>;
    const text = this.props.text &&
      <Text style={styles.text}>{this.props.text}</Text>;

    return (
      <View style={[styles.container, this.props.style]}>
        {image}
        {title}
        {text}
        {this.props.children}
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  title: {
    textAlign: 'center',
    color: AppColors.inactiveText,
    fontSize: 25,
    lineHeight: 28,
    padding: 35,
  },
  text: {
    textAlign: 'center',
    color: AppColors.inactiveText,
  },
});

module.exports = EmptyList;
