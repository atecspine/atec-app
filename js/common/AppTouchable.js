//@flow
'use strict';

import React, {
  TouchableHighlight,
  TouchableNativeFeedback,
  Platform,
} from 'react-native';
import AppColors from './AppColors';

function AppTouchableIOS(props) {
  return (
    <TouchableHighlight
      accessibilityTraits="button"
      underlayColor={AppColors.actionText}
      {...props}
    />
  );
}

const AppTouchable = Platform.OS === 'android'
  ? TouchableNativeFeedback
  : AppTouchableIOS;

module.exports = AppTouchable;
