//@flow

import React, {Component} from 'React';
import {
  StyleSheet,
  View,
} from 'react-native';
import AppColors from './AppColors';

class ItemsWithSeparator extends Component {
  render() {
    var children = [];
    var length = React.Children.count(this.props.children.filter((child) => child));
    React.Children.forEach(
      this.props.children.filter((child) => child),
      (child, ii) => {
        children.push(child);
        if (ii !== length - 1 && child) {
          children.push(
            <View
              key={'separator-' + ii}
              style={[styles.separator, this.props.separatorStyle]}
            />
          );
        }
      }
    );
    return (
      <View style={this.props.style}>
        {children}
      </View>
    );
  }
}

var styles = StyleSheet.create({
  separator: {
    backgroundColor: AppColors.cellBorder,
    height: 0.5,
  },
});

module.exports = ItemsWithSeparator;
