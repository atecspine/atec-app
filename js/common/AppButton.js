import AppColors from './AppColors';
import React, {Component} from 'React';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Image,
} from 'react-native';

class AppButton extends Component {
  render() {
    const caption = this.props.caption;
    let icon;
    if (this.props.icon) {
      icon = <Image source={this.props.icon} style={this.props.type === 'fab' ? styles.iconRound : styles.icon} />;
    }
    let content;
    if (this.props.type === 'primary') {
      content = (
        <View
          style={[styles.button, styles.primaryButton, this.props.buttonStyle]}
        >
          {icon}
          <Text style={[styles.caption, styles.primaryCaption]}>
            {caption.toUpperCase()}
          </Text>
        </View>
      );
    } else if (this.props.type === 'bordered') {
      content = (
        <View style={[styles.button, styles.border, this.props.buttonStyle]}>
          {icon}
          <Text style={[styles.caption, styles.borderedCaption, this.props.captionStyle]}>
            {caption}
          </Text>
        </View>
      );
    } else if (this.props.type === 'fab') {
      content = (
        <View
          style={[styles.roundButton, this.props.buttonStyle]}
        >
          {icon}
          <Text style={styles.roundCaption}>
            {caption}
          </Text>
        </View>
      );
    } else {
      content = (
        <View style={[styles.button, styles.default, this.props.buttonStyle]}>
          {icon}
          <Text style={[styles.caption, this.props.captionStyle]}>
            {caption.toUpperCase()}
          </Text>
        </View>
      );
    }
    return (
      <TouchableOpacity
        accessibilityTraits="button"
        onPress={this.props.onPress}
        activeOpacity={0.8}
        style={[this.props.type !== 'fab' ? styles.container : null, this.props.style]}>
        {content}
      </TouchableOpacity>
    );
  }
}

const HEIGHT = 45;

var styles = StyleSheet.create({
  container: {
    marginVertical: 5,
    height: HEIGHT,
  },
  button: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  border: {
    borderWidth: 1,
    borderColor: AppColors.secondaryButton,
    borderRadius: 3,
  },
  primaryButton: {
    borderRadius: 3,
    backgroundColor: AppColors.primaryButton,
  },
  roundButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 65,
    width: 65,
    borderRadius: 65 / 2,
    shadowColor: 'rgba(0, 0, 0, 0.5)',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 0},
    shadowRadius: 4,
  },
  default: {
    borderWidth: 1,
    borderRadius: 3,
    backgroundColor: AppColors.gray,
    borderColor: AppColors.cellBorder,
  },
  icon: {
    marginRight: 12,
  },
  iconRound: {
    width: 23,
    height: 23,
  },
  caption: {
  },
  primaryCaption: {
    fontFamily: 'Soleto',
    color: AppColors.darkBlue,
    fontSize: 16,
    letterSpacing: 1,
    fontWeight: 'bold',
  },
  secondaryCaption: {
    fontFamily: 'Soleto',
    color: AppColors.actionText,
  },
  borderedCaption: {
    fontFamily: 'Soleto',
    fontWeight: 'bold',
    color: AppColors.secondaryButton,
  },
  roundCaption: {
    backgroundColor: 'transparent',
    color: 'white',
    fontWeight: '600',
    fontSize: 12,
  }
});

module.exports = AppButton;
