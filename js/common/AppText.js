//@flow

import React from 'React';
import {
  StyleSheet,
  Text,
} from 'react-native';
import AppColors from './AppColors';
import {normalize} from '../util/normalize';

export function AppText({style, ...props}) {
  return <Text style={[styles.font, style]} {...props} />;
}

export function Heading1({style, ...props}) {
  return <Text style={[styles.font, styles.h1, style]} {...props} />;
}

export function Heading2({style, ...props}) {
  return <Text style={[styles.font, styles.h2, style]} {...props} />;
}

export function Heading3({style, ...props}) {
  return <Text style={[styles.font, styles.h3, style]} {...props} />;
}

export function Paragraph({style, ...props}) {
  return <Text style={[styles.font, styles.p, style]} {...props} />;
}

export function HelpText({style, ...props}) {
  return <Text style={[styles.font, styles.help, style]} {...props} />;
}

const styles = StyleSheet.create({
  font: {
    fontFamily: 'Soleto'
  },
  h1: {
    fontFamily: 'Soleto',
    fontSize: normalize(28),
    lineHeight: normalize(32),
    color: AppColors.darkText,
    fontWeight: '500',
    color: AppColors.darkBlue,
  },
  h2: {
    fontFamily: 'Soleto',
    fontSize: normalize(24),
    lineHeight: normalize(28),
    color: AppColors.darkBlue,
  },
  h3: {
    fontFamily: 'Soleto',
    fontSize: normalize(20),
    lineHeight: normalize(24),
    color: AppColors.darkBlue,
  },
  p: {
    fontFamily: 'Soleto',
    fontSize: normalize(16),
    lineHeight: normalize(20),
    color: AppColors.darkBlue,
  },
  help: {
    fontFamily: 'Soleto',
    fontSize: normalize(13),
    lineHeight: normalize(17),
    color: AppColors.inactiveText,
  }
});
