//@flow

const colors = {
  black: '#333333',
  darkBlack: '#222222',
  offWhite: '#EFEFEF',
  lightGray: '#CCCCCC',
  lighterGray: '#DDDDDD',
  gray: '#999999',
  darkGray: '#666666',
  darkerGray: '#555555',
  red: '#FF0000',
  lightRed: '#FCE8E8',
  white: '#FFFFFF',
  // ATEC colors
  darkBlue: '#001535',
  blueGray: '#7F8A9A',
  brightGreen: '#00FF00',
};

module.exports = {
  // Color uses
  actionText: colors.brightGreen,
  inactiveText: colors.lightGray,
  badgeColor: colors.red,
  darkText: colors.black,
  lightText: colors.gray,
  cellBorder: colors.lightGray,
  darkBackground: colors.black,
  tableViewBackground: colors.lightGray,
  primaryButton: colors.brightGreen,
  secondaryButton: colors.darkBlue,
  headerBackground: colors.darkBlue,
  mediumText: colors.darkGray,
  tabBarBackground: colors.darkBlue,
  // general colors
  brightGreen: colors.brightGreen,
  blue: colors.blue,
  darkBlue: colors.darkBlue,
  black: colors.black,
  darkBlack: colors.darkBlack,
  offWhite: colors.offWhite,
  lightGray: colors.lightGray,
  lighterGray: colors.lighterGray,
  gray: colors.gray,
  darkGray: colors.darkGray,
  darkerGray: colors.darkerGray,
  red: colors.red,
  lightRed: colors.lightRed,
  white: colors.white,
  blueGray: colors.blueGray,
};
