import React from 'react';
import { 
  StyleSheet, 
  Text,
  View,
  ActivityIndicator,
  Image,
} from 'react-native';
import AppColors from './AppColors';
import {normalize} from '../util/normalize';

class FileImage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let downloadedIcon;
    let {file_type, offline_path } = this.props.file;
    let image = this.props.thumbnail_url 
      ? <Image resizeMode="contain" style={styles.cellImage} source={{uri: this.props.thumbnail_url}} height={normalize(100)} width={normalize(140)} />
      : (<View style={{justifyContent: 'center', alignItems: 'center', height: normalize(110), width: normalize(140)}}>
          <ActivityIndicator />
        </View>);
    if (offline_path) {
      downloadedIcon = <Image style={styles.downloadedIcon} source={require('./img/downloaded.png')} />;
    }
    
    return (
      <View style={styles.cell}>
        <View style={styles.content}>
          {downloadedIcon}
          {image}
          <View style={styles.cellDescription}>
            <Text style={styles.cellDescriptionText}>{file_type}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cell: {
    backgroundColor: 'white',
    width: normalize(150),
    height: normalize(150),
    marginVertical: 15,
    marginRight: 15,
    borderWidth: 1,
    borderColor: AppColors.brightGreen,
    borderRadius: 6,
  },
  cellImage: {
    marginVertical: 5,
    marginHorizontal: 10,
    alignSelf: 'center',
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'space-between',
    backgroundColor: AppColors.lighterGray,
    borderRadius: 6,
  },
  downloadedIcon: {
    position: 'absolute',
    top: -8,
    right: -8,
    width: 20,
    height: 20,
    zIndex: 2,
  },
  cellDescription: {
    height: normalize(35),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: AppColors.darkBlue,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
    padding: 5,
  },
  cellDescriptionText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: 'Soleto',
    fontWeight: '500',
    fontSize: normalize(14),
  }
});

module.exports = FileImage;
