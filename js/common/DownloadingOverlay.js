import React from 'react';
import { 
  StyleSheet, 
  Text,
  View,
} from 'react-native';
import AppColors from './AppColors';
import { connect } from 'react-redux';
import ProgressBar from 'react-native-progress/Bar';
import _ from 'lodash';
import {hideDownloadingOverlay} from '../actions';

class DownloadingOverlay extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (!this.props.visible) {
      return null;
    }

    const downloadProgress = this.props.totalBytes === 0 ? 0.01 : _.floor(this.props.bytesTransferred / this.props.totalBytes, 2);
    const downloadPercent = _.floor(downloadProgress * 100);

    let content;
    if (downloadProgress === 1) {
      setTimeout(() => {
        this.props.dispatch(hideDownloadingOverlay()); 
      }, 2000);
      content = (
        <View style={styles.completedView}>
          <Text style={styles.completedText}>Download Complete!</Text>
        </View>
      );
    } else {
      content = (
        <View style={styles.downloadingWrap}>
          <Text style={styles.overlayText}>Downloading {this.props.fileCount > 1 ? `${this.props.fileCount} files` : null}</Text>
          <View style={styles.progressBarWrap}>
            <ProgressBar 
              width={null}
              height={10}
              borderRadius={0}
              useNativeDriver={true}
              animationType="timing"
              animated={true}
              progress={downloadProgress}
              color={AppColors.brightGreen}
              borderColor={AppColors.darkBlue}
            />
          </View>
          <Text style={styles.progressText}>{downloadPercent < 1 ? 1 : downloadPercent}%</Text>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        {content}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: AppColors.blueGray,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingHorizontal: 10,
    paddingVertical: 5,
    alignItems: 'center',
  },
  downloadingWrap: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  overlayText: {
    marginRight: 10,
    color: AppColors.darkBlue,
    fontFamily: 'Soleto',
  },
  progressBarWrap: {
    flex: 1,
  },
  progressText: {
    marginLeft: 10,
    fontFamily: 'Soleto',
    color: AppColors.darkBlue,
  },
  completedView: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  completedText: {
    fontFamily: 'Soleto',
    color: AppColors.brightGreen,
  },
});

function select(state) {
  return {
    visible: state.downloadingOverlay.visible,
    fileCount: state.downloadingOverlay.files.length,
    bytesTransferred: _.reduce(state.downloadingOverlay.files, (sum, f) => sum + f.bytesTransferred, 0),
    totalBytes: _.reduce(state.downloadingOverlay.files, (sum, f) => sum + f.totalBytes, 0),
  };
}

module.exports = connect(select)(DownloadingOverlay);
