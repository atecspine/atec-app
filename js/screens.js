import React from 'react';
import {Navigation} from 'react-native-navigation';

import LoginScreen from './login/LoginScreen';
import ForgotPasswordView from './login/ForgotPasswordView';
import HomeView from './tabs/products/HomeView';
import ProductsView from './tabs/products/ProductsView';
import ProductDetailsView from './tabs/products/ProductDetailsView';
import SearchView from './tabs/search/SearchView';
import FavoritesView from './tabs/favorites/FavoritesView';
import ContactView from './tabs/contact/ContactView';
import HistoryView from './tabs/history/HistoryView';
import NewslettersView from './tabs/newsletters/NewslettersView';
import HorizontalProductsDisplayView from './tabs/products/HorizontalProductsDisplayView';
import ProductCell from './tabs/products/ProductCell';
import FileCell from './tabs/products/FileCell';
import FileView from './tabs/products/FileView';
import FolderView from './tabs/favorites/FolderView';
import DownloadingOverlay from './common/DownloadingOverlay';

// register all screens of the app (including internal ones)
export function registerScreens(store, Provider) {
  Navigation.registerComponent('atec.LoginScreen', () => LoginScreen, store, Provider);
  Navigation.registerComponent('atec.ForgotPasswordView', () => ForgotPasswordView, store, Provider);
  Navigation.registerComponent('atec.HorizontalProductsDisplayView', () => HorizontalProductsDisplayView, store, Provider);
  Navigation.registerComponent('atec.ProductCell', () => ProductCell, store, Provider);
  Navigation.registerComponent('atec.FileCell', () => FileCell, store, Provider);
  Navigation.registerComponent('atec.DownloadingOverlay', () => DownloadingOverlay, store, Provider);

  Navigation.registerComponent('atec.HomeView', () => HomeView, store, Provider);
  Navigation.registerComponent('atec.ProductsView', () => ProductsView, store, Provider);
  Navigation.registerComponent('atec.ProductDetailsView', () => ProductDetailsView, store, Provider);
  Navigation.registerComponent('atec.SearchView', () => SearchView, store, Provider);
  Navigation.registerComponent('atec.FavoritesView', () => FavoritesView, store, Provider);
  Navigation.registerComponent('atec.ContactView', () => ContactView, store, Provider);
  Navigation.registerComponent('atec.HistoryView', () => HistoryView, store, Provider);
  Navigation.registerComponent('atec.FileView', () => FileView, store, Provider);
  Navigation.registerComponent('atec.FolderView', () => FolderView, store, Provider);
  Navigation.registerComponent('atec.NewslettersView', () => NewslettersView, store, Provider);
}
