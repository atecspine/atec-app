import firebase from 'react-native-firebase';

function track(action) {
  switch (action.type) {
    case 'LOGGED_IN':
      firebase.analytics().logEvent('logged_in');
      break;
    case 'LOGGED_OUT':
      firebase.analytics().logEvent('logged_out');
      break;
    case 'ADD_FAVORITE_FILE':
      firebase.analytics().logEvent('added_favorite_file', {file_url: action.file.file_url});
      break;
    case 'REMOVE_FAVORITE_FILE':
      firebase.analytics().logEvent('removed_favorite_file', {file_url: action.file.file_url});
      break;      
    case 'FILE_ADDED_TO_HISTORY':
      firebase.analytics().logEvent('viewed_file', {file_url: action.file.file_url});
      break;
    case 'SEARCH_QUERY_UPDATED':
      firebase.analytics().logEvent('searched', {query: action.query});
      break;
  }
}

module.exports = track;
