// @flow
import {applyMiddleware, createStore, compose} from 'redux';
import thunk from 'redux-thunk';
import reducers from '../reducers';
import analytics from './analytics';
import promise from './promise';
import array from './array';
import {createLogger} from 'redux-logger';
import {persistStore, persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';

var isDebuggingInChrome = __DEV__ && !!window.navigator.userAgent;

var logger = createLogger({
  predicate: (getState, action) => isDebuggingInChrome,
  collapsed: true,
  duration: true,
});

// keys for blacklisting and purging
const blacklist = ['search', 'sharing', 'downloadingOverlay'];
let enhancer;
if (isDebuggingInChrome) {
  enhancer = compose(
    applyMiddleware(thunk, promise, array, analytics, logger)
  );
} else {
  enhancer = compose(
    applyMiddleware(thunk, promise, array, analytics)
  );
}

function configureStore() {
  const persistConfig = {
    key: 'atec',
    storage,
    blacklist: blacklist
  };

  const persistedReducer = persistReducer(persistConfig, reducers);
  const store = createStore(persistedReducer, {}, enhancer);
  return store;
}

module.exports = configureStore;
