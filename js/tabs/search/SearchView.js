import React from 'react';
import { TextInput, FlatList, View } from 'react-native';
import AppColors from '../../common/AppColors';
import { createStylesheet } from '../../common/AppStyleSheet';
import PureListView from '../../common/PureListView';
import EmptyList from '../../common/EmptyList';
import { Heading1 } from '../../common/AppText';
import SearchResultCell from './SearchResultCell';

import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import {
  updateSearchQuery,
} from '../../actions';

import {
  filterResults
} from '../../util/search';

import _ from 'lodash';

import AppBaseScreeen from '../../AppBaseScreen';

class SearchView extends AppBaseScreeen {
  static navigatorButtons = {
    leftButtons: [
      {
        title: 'Close',
        icon: require('../../common/img/x.png'),
        id: 'close',
      },
    ]
  };

  constructor(props) {
    super(props);

    this.state = {
      showShadow: false,
    }

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this._onScroll = this._onScroll.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.renderEmptyList = this.renderEmptyList.bind(this);
    this.updateSearchQuery = this.updateSearchQuery.bind(this);
    this.close = this.close.bind(this);
  }

  onNavigatorEvent(event) {
    if (event.type == 'NavBarButtonPress') {
      if (event.id == 'close') {
        this.close();
      }
    }
  }

  updateSearchQuery(query) {
    this.props.dispatch(updateSearchQuery(query));
  }

  renderEmptyList() {
    return (
      <EmptyList
        title={this.props.searchQuery !== '' ? 'No search results found' : null}
      />
    );
  }

  renderRow({item}) {
    // Look to see if this file has been saved, if it has
    // use the saved file instead (uses offline storage)
    let savedFile = _.find(this.props.savedFiles, ['file_url', item.file_url]);
    if (savedFile) {
      item = savedFile;
    }
  
    return (
      <SearchResultCell 
        file={item} 
        navigator={this.props.navigator} 
      />
    );
  }

  _onScroll(event) {
    var currentOffset = event.nativeEvent.contentOffset.y;
    if (currentOffset > 0) {
      this.setState({showShadow: true});
    } else {
      this.setState({showShadow: false});
    }
  }

  close() {
    this.props.dispatch(updateSearchQuery(''));
    this.props.navigator.dismissModal();
  }

  renderContent() {
    return (
      <View style={styles.container}>
        <View style={[styles.viewWrap, styles.shadow, this.state.showShadow ? {zIndex: 1} : {}]}>
          <Heading1 style={styles.headingText}>Search</Heading1>
          <View style={styles.searchInputWrap}>
            <TextInput
              ref="searchField"
              autoFocus={true}
              placeholder="Enter search..."
              multiline={false}
              keyboardType="default"
              clearButtonMode="while-editing"
              returnKeyType="search"
              onChangeText={(text) => this.updateSearchQuery(text)}
              style={styles.textField}
              value={this.props.searchQuery}
              underlineColorAndroid="transparent"
            />
          </View>
        </View>
        <FlatList
          data={this.props.searchResults}
          renderItem={this.renderRow}
          ListEmptyComponent={this.renderEmptyList}
          ItemSeparatorComponent={() => <View style={styles.separator} />}
          style={styles.listView}
          onScroll={this._onScroll}
          keyExtractor={(item, index) => item.file_url + index}
        />
      </View>
    );
  }
}

const INPUT_HEIGHT = 45;
const styles = createStylesheet({
  container: {
    flex: 1,
    backgroundColor: AppColors.white,
    borderColor: AppColors.brightGreen,
    borderTopWidth: 2,
  },
  viewWrap: {
    padding: 25,
  },
  separator: {
    backgroundColor: AppColors.darkBlue,
    height: 0.75,
  },
  listView: {
    flex: 1,
    backgroundColor: 'white',
  },
  shadow: {
    shadowColor: 'rgba(0, 0, 0, 0.2)',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 4},
    shadowRadius: 4,
  },
  searchInputWrap: {
    position: 'relative',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: AppColors.brightGreen,
    marginVertical: 5,
    maxWidth: 550,
  },
  textField: {
    fontFamily: 'Soleto',
    fontSize: 16,
    height: INPUT_HEIGHT,
    paddingHorizontal: 15,
    paddingVertical: 10,
    android: {
      textAlignVertical: 'top',
    },
  }
});

const filteredResults = createSelector(
  (store) => store.products.list,
  (store) => store.newsletters.list,
  (store) => store.search.query,
  (products, newsletters, searchQuery) => filterResults(products, newsletters, searchQuery),
);

function select(state) {
  return {
    savedFiles: state.favorites.list,
    searchResults: filteredResults(state),
    searchQuery: state.search.query,
  };
}

module.exports = connect(select)(SearchView);
