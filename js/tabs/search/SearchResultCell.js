import React from 'react';
import { 
  StyleSheet,
  View,
  TouchableHighlight,
} from 'react-native';
import FileImage from '../../common/FileImage';
import { Heading3 } from '../../common/AppText';
import firebase from 'react-native-firebase';
import {connect} from 'react-redux';

import {
  addFileToHistory,
} from '../../actions';

class SearchResultCell extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      thumbnail_url: null,
      file_url: null,
    };

    this.showFile = this.showFile.bind(this);
  }

  componentDidMount() {
    let {file_thumbnail_url, file_url, offline_path, offline_thumbnail_path} = this.props.file;
    
    if (offline_thumbnail_path) {
      let thumbnail_path = `${firebase.storage.Native.CACHES_DIRECTORY_PATH}/${file_thumbnail_url}`;
      this.setState({thumbnail_url: thumbnail_path});
    } else {
      const file_thumbnail_ref = firebase.storage().ref(file_thumbnail_url);
      file_thumbnail_ref.getDownloadURL().then((url) => {
        this.setState({thumbnail_url: url});
      });
    }
  
    if (offline_path) {
      let file_path = `${firebase.storage.Native.CACHES_DIRECTORY_PATH}/${file_url}`;
      this.setState({file_url: file_path});
    } else {
      const file_ref = firebase.storage().ref(file_url);
      file_ref.getDownloadURL().then((url) => {
        this.setState({file_url: url});
      });
    }
  }

  showFile() {
    if (this.state.file_url) {
      this.props.dispatch(addFileToHistory(this.props.file));
      this.props.navigator.push({
        screen: 'atec.FileView',
        title: this.props.file.file_type,
        backButtonTitle: '',
        passProps: {
          file: this.props.file,
          file_url: this.state.file_url,
        },
      });
    } else {
      alert('There was an issue downloading the file. Please check your connection and try again.');
    }
  }

  render() {
    let {file_name} = this.props.file;

    return (
      <TouchableHighlight onPress={this.showFile}>
        <View style={styles.cell}>
          <FileImage file={this.props.file} thumbnail_url={this.state.thumbnail_url} />
          <Heading3 style={styles.cellTitle}>{file_name}</Heading3>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  cell: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    // paddingVertical: 15,
    paddingHorizontal: 25,
  },
  cellTitle: {
    flex: 1,
    paddingLeft: 15,
  }
});

module.exports = connect()(SearchResultCell);
