import React from 'react';
import {StyleSheet, View, ScrollView, Image, TouchableOpacity, Alert} from 'react-native';
import _ from 'lodash';
import {connect} from 'react-redux';
import prompt from 'react-native-prompt-android';
import {MultiPickerMaterialDialog} from 'react-native-material-dialog';

import AppColors from '../../common/AppColors';
import PureListView from '../../common/PureListView';
import EmptyList from '../../common/EmptyList';
import {Heading1} from '../../common/AppText';
import FavoritesCell from './FavoritesCell';
import FolderCell from './FolderCell';
import {Heading3} from '../../common/AppText';

import {
  resetFileSharing,
  toggleFileToShare,
  toggleFolderToShare,
  addFolder,
  addFilesToFolders,
  resetFolderSharing,
  removeFolders
} from '../../actions';

import AppBaseScreeen from '../../AppBaseScreen';
import {email} from '../../util/mail';

class FavoritesView extends AppBaseScreeen {
  static navigatorButtons = {
    leftButtons: [
      {
        id: 'search',
        systemItem: 'search'
      }
    ],
  }

  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      showShadow: false,
      folderPickerVisible: false
    };

    this._onScroll = this._onScroll.bind(this);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.cancelSelect = this.cancelSelect.bind(this);
    this.toggleFileSelected = this.toggleFileSelected.bind(this);
    this.renderEmptyFolderList = this.renderEmptyFolderList.bind(this);
    this.renderEmptyFileList = this.renderEmptyFileList.bind(this);
    this.renderFile = this.renderFile.bind(this);
    this.renderFolder = this.renderFolder.bind(this);
    this.onAddFolder = this.onAddFolder.bind(this);
    this.toggleFolderSelected = this.toggleFolderSelected.bind(this);
    this.handleRemoveFolder = this.handleRemoveFolder.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.isVisible;
  }

  onNavigatorEvent(event) {
    switch (event.id) {
      case 'search':
        this.props.navigator.showModal({
          screen: 'atec.SearchView',
          title: 'Search',
          titleImage: require('../../common/img/header-logo.png'),
          passProps: {}
        });
      case 'add':
        this.setState({folderPickerVisible: true});
        break;
      case 'remove':
        this.handleRemoveFolder();
        break;
      case 'share':
        this.shareSelected();
        break;
      case 'cancel':
        this.cancelSelect();
        break;
      case 'willAppear':
        this.setState({isVisible: true});
        break;
      case 'willDisappear':
        this.setState({isVisible: false});
        break;
    }
  }

  renderEmptyFolderList() {
    return <EmptyList title={'No Folders added'} />;
  }

  renderEmptyFileList() {
    return <EmptyList title={'No files saved'} />;
  }

  renderFile(file) {
    const {navigator, selectedFiles} = this.props;
    let urls = _.map(selectedFiles, 'file_url');
    let selected = _.includes(urls, file.file_url);
    return (
      <FavoritesCell
        key={file.file_url}
        file={file}
        selected={selected}
        navigator={navigator}
        onSelect={() => this.toggleFileSelected(file)}
      />
    );
  }

  renderFolder(folder) {
    const {navigator} = this.props;
    return (
      <FolderCell
        key={folder}
        folder={folder}
        navigator={navigator}
        onSelect={() => this.toggleFolderSelected(folder)}
      />
    );
  }

  toggleFolderSelected(folder) {
    this.props.dispatch(toggleFolderToShare(folder));
  }

  toggleFileSelected(file) {
    this.props.dispatch(toggleFileToShare(file));
  }

  handleRemoveFolder() {
    const folders = this.props.selectedFolders;
    Alert.alert(
      'Are you sure?',
      'This will remove the folder and file organization but it will not remove files from Favorites.',
      [
        {text: 'Cancel', onPress: () => null, style: 'cancel'},
        {text: 'Remove', onPress: () => {
          this.props.dispatch(removeFolders(folders));
          this.cancelSelect();
        }, style: 'destructive'}
      ]
    );
  }

  cancelSelect() {
    this.props.dispatch(resetFileSharing());
    this.props.dispatch(resetFolderSharing());
  }

  shareSelected() {
    let filesToShare = [];
    if (!_.isEmpty(this.props.selectedFiles) && !_.isEmpty(this.props.selectedFolders)) {
      const folders = this.props.selectedFolders;
      let fileNames = _.map(this.props.folderMap.filter(item => _.includes(folders, item.folder)), 'file');
      const files = this.props.favorites.filter(item => _.includes(fileNames, item.file_name));
      filesToShare = [
        ...this.props.selectedFiles,
        ...files
      ];
      filesToShare = _.uniq(filesToShare);
    } else if (!_.isEmpty(this.props.selectedFiles)) {
      filesToShare = this.props.selectedFiles;
    } else if (!_.isEmpty(this.props.selectedFolders)) {
      const folders = this.props.selectedFolders;
      let fileNames = _.map(this.props.folderMap.filter(item => _.includes(folders, item.folder)), 'file');
      const files = this.props.favorites.filter(item => _.includes(fileNames, item.file_name));
      filesToShare = files;
    }

    email(filesToShare, (error, event) => {
      if (error) {
        alert(error);
      }
      this.props.dispatch(resetFileSharing());
      this.props.dispatch(resetFolderSharing());
    });
  }

  _onScroll(event) {
    var currentOffset = event.nativeEvent.contentOffset.y;
    if (currentOffset > 0) {
      this.setState({showShadow: true});
    } else {
      this.setState({showShadow: false});
    }
  }

  onAddFolder() {
    prompt(
      'Create Folder',
      'To create a new folder, choose a unique name for it.',
      [
        {text: 'Cancel', onPress: () => {}, style: 'cancel'},
        {text: 'Add', onPress: (folder) => {
          if (_.includes(this.props.folders, folder)) {
            alert('Folder name has already been used!');
            return;
          }  
          if (folder === '') {
            alert('Folder name cannot be empty!');
            return;
          }
          this.props.dispatch(addFolder(folder));
        }}
      ],
      {
        cancelable: false,
        defaultValue: '',
        placeholder: 'Enter Folder Name'
      }
    );
  }

  handleFolderChosen = result => {
    const multiPickerSelectedItems = result.selectedItems;
    this.setState({
      folderPickerVisible: false,
      multiPickerSelectedItems
    });

    const {selectedFiles} = this.props;
    this.props.dispatch(addFilesToFolders(
      selectedFiles.map(file => file.file_name),
      multiPickerSelectedItems.map(item => item.label)
    ));
    this.props.dispatch(resetFileSharing());
  }

  getFolderItems = () => this.props.folders.map((row, index) => ({value: index, label: row}))

  renderNavBar = () => {
    let leftButtons, rightButtons;
    const {selectedFiles, selectedFolders, navigator, folders} = this.props;
    if (!_.isEmpty(selectedFiles) || !_.isEmpty(selectedFolders)) {
      leftButtons = [{
        title: 'Cancel',
        id: 'cancel',
        systemItem: 'cancel'
      }];
      rightButtons = [{
        icon: require('../../common/img/share.png'),
        id: 'share'
      }];

      if (selectedFiles.length >= 1 && selectedFolders.length === 0 && folders.length > 0) {
        rightButtons = [{
          id: 'add',
          systemItem: 'add'
        }, ...rightButtons];
      }

      if (selectedFolders.length >= 1 && selectedFiles.length === 0) {
        rightButtons = [{
          id: 'remove',
          systemItem: 'trash'
        }, ...rightButtons];
      }
    } else {
      rightButtons = [{
        id: 'search',
        systemItem: 'search'
      }];
      leftButtons = [];
    }
    navigator.setButtons({
      leftButtons: leftButtons,
      rightButtons: rightButtons
    });
  }

  renderContent() {
    const {folders, favorites} = this.props;

    this.renderNavBar();

    return (
      <View style={styles.container}>
        <View style={[styles.viewWrap, styles.shadow, this.state.showShadow ? {zIndex: 1} : {}]}>
          <Heading1 style={styles.headingText}>Favorites</Heading1>
        </View>
        <ScrollView style={{backgroundColor: AppColors.white}} onScroll={this._onScroll}>
            <View style={styles.section}>
            <Heading3>Folders</Heading3>
            <TouchableOpacity onPress={this.onAddFolder}>
              <Image style={styles.addImage} source={require('../../common/img/add-circle-icon.png')} />
            </TouchableOpacity>
          </View>
          <PureListView
            data={folders}
            renderRow={this.renderFolder}
            renderEmptyList={this.renderEmptyFolderList}
            renderSeparator={true}
            alwaysBounceVertical={false}
            style={styles.listView}
            tabBarSpace={false}
          />
          <Heading3 style={styles.section}>Files</Heading3>
          <PureListView
            data={favorites}
            renderRow={this.renderFile}
            renderEmptyList={this.renderEmptyFileList}
            renderSeparator={true}
            alwaysBounceVertical={false}
            style={styles.listView}
            tabBarSpace={false}
          />
        </ScrollView>
        {folders && (
          <MultiPickerMaterialDialog
            title='Choose Folder(s)'
            colorAccent={AppColors.darkBlue}
            items={this.getFolderItems()}
            visible={this.state.folderPickerVisible}
            onCancel={() => this.setState({folderPickerVisible: false})}
            onOk={this.handleFolderChosen}
          />
        )}
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppColors.white,
    borderColor: AppColors.brightGreen,
    borderTopWidth: 2
  },
  viewWrap: {
    padding: 25,
    paddingBottom: 15
  },
  separator: {
    backgroundColor: AppColors.darkBlue,
    height: 0.75,
    marginTop: 25
  },
  listView: {
    flex: 1,
    backgroundColor: 'white'
  },
  shadow: {
    shadowColor: 'rgba(0, 0, 0, 0.2)',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 4},
    shadowRadius: 4
  },
  section: {
    flexDirection: 'row',
    marginLeft: 25,
    marginTop: 20,
    marginBottom: 10,
    alignItems: 'center'
  },
  addImage: {
    marginLeft: 6,
    width: 24,
    height: 24
  },
});

function select(state) {
  return {
    selectedFiles: state.sharing.selectedFiles,
    selectedFolders: state.sharing.selectedFolders,
    favorites: _.orderBy(state.favorites.list, ({added_on}) => added_on || '', ['desc']),
    folders: state.folders,
    folderMap: state.folderMap
  };
}

export default connect(select)(FavoritesView);
