import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';
import {Heading3} from '../../common/AppText';
import {normalize} from '../../util/normalize';
import _ from 'lodash';
import {connect} from 'react-redux';

class FolderCell extends React.Component {
  showFolder = () => {
    const {navigator, folder} = this.props;
    navigator.push({
      screen: 'atec.FolderView',
      titleImage: require('../../common/img/header-logo.png'),
      backButtonTitle: '',
      passProps: {
        folder,
      }
    });
  }

  render() {
    const {folder} = this.props;
    const {selectedFolders} = this.props;
    let selected = _.includes(selectedFolders, folder);

    let checkmark;
    let check = selected
      ? <Image style={styles.check} source={require('../../common/img/checkmark_filled.png')} />
      : <Image style={styles.check} source={require('../../common/img/checkmark_empty.png')} />;
    checkmark = (
      <TouchableOpacity onPress={this.props.onSelect} style={styles.checkmark}>
        {check}
      </TouchableOpacity>
    );
    return (
      <TouchableHighlight onPress={this.showFolder}>
        <View style={styles.cell}>
          {checkmark}
          <View style={styles.details}>
            <Heading3>{folder}</Heading3>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  cell: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingVertical: 10,
    paddingHorizontal: 25
  },
  details: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  checkmark: {
    width: normalize(30),
    height: normalize(30),
    marginRight: normalize(15)
  }
});

function select(state) {
  return {
    selectedFolders: state.sharing.selectedFolders
  };
}


module.exports = connect(select)(FolderCell);
