import React from 'react';
import {View, Alert} from 'react-native';
import {connect} from 'react-redux';

import PureListView from '../../common/PureListView';
import EmptyList from '../../common/EmptyList';
import FavoritesCell from './FavoritesCell';
import {Heading1} from '../../common/AppText';

import _ from 'lodash';

import AppBaseScreeen from '../../AppBaseScreen';
import {email} from '../../util/mail';
import {styles} from './FavoritesView';
import {
  removeFilesFromFolder
} from '../../actions';

class FolderView extends AppBaseScreeen {  
  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      showShadow: false,
      selectedFiles: []
    };

    this._onScroll = this._onScroll.bind(this);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.cancelSelect = this.cancelSelect.bind(this);
    this.toggleFileSelected = this.toggleFileSelected.bind(this);
    this.removeFiles = this.removeFiles.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.isVisible;
  }

  onNavigatorEvent(event) {
    switch (event.id) {
      case 'share':
        email(this.state.selectedFiles, (error, event) => {
          if (error) {
            alert(error);
          }
          this.resetFileSharing();
        });
        break;
      case 'remove':
        this.removeFiles();
        break;
      case 'cancel':
        this.cancelSelect();
        break;
      case 'willAppear':
        this.setState({isVisible: true});
        break;
      case 'willDisappear':
        this.setState({isVisible: false});
        break;
    }
  }

  renderEmptyFileList = () => <EmptyList title={'There are no files in this folder'} />

  renderFile = (file) => {
    const {selectedFiles} = this.state;
    let urls = _.map(selectedFiles, 'file_url');
    let selected = _.includes(urls, file.file_url);
    return (
      <FavoritesCell
        key={file.file_url}
        file={file}
        selected={selected}
        navigator={this.props.navigator}
        onSelect={() => this.toggleFileSelected(file)}
      />
    );
  }

  removeFiles() {
    Alert.alert(
      'Are you sure?',
      'This will remove this file from this folder but not from Favorites.',
      [
        {text: 'Cancel', onPress: () => null, style: 'cancel'},
        {text: 'Remove', onPress: () => {
          const files = _.map(this.state.selectedFiles, 'file_name');
          this.props.dispatch(removeFilesFromFolder(files, this.props.folder));
          this.cancelSelect();
        }, style: 'destructive'}
      ]
    );
  }

  toggleFileSelected(file) {
    const {selectedFiles} = this.state;
    const others = selectedFiles.filter(item => item.file_url !== file.file_url);
    if (selectedFiles.length !== others.length) {
      this.setState({selectedFiles: others});
    } else {
      this.setState({selectedFiles: [...selectedFiles, file]});
    }
  }

  cancelSelect() {
    this.setState({selectedFiles: []});
  }

  _onScroll(event) {
    var currentOffset = event.nativeEvent.contentOffset.y;
    if (currentOffset > 0) {
      this.setState({showShadow: true});
    } else {
      this.setState({showShadow: false});
    }
  }

  renderContent() {
    let leftButtons, rightButtons;
    const {navigator, favorites, folderMap, folder} = this.props;
    const fileNames = folderMap.filter(item => item.folder === folder).map(item => item.file);
    const files = favorites.filter(item => fileNames.find(name => item.file_name === name));

    const {selectedFiles} = this.state;
    if (!_.isEmpty(selectedFiles)) {
      leftButtons = [{
        title: 'Cancel',
        id: 'cancel',
        systemItem: 'cancel'
      }];
      rightButtons = [{
        id: 'remove',
        systemItem: 'trash'
      }, {
        icon: require('../../common/img/share.png'),
        systemItem: 'action',
        id: 'share'
      }];
    } else {
      leftButtons = [];
      rightButtons = [{
        id: 'search',
        systemItem: 'search'
      }];
    }
    navigator.setButtons({
      leftButtons: leftButtons,
      rightButtons: rightButtons
    });

    return (
      <View style={styles.container}>
        <View style={[styles.viewWrap, styles.shadow, this.state.showShadow ? {zIndex: 1} : {}]}>
          <Heading1 style={styles.headingText}>{this.props.folder}</Heading1>
        </View>
        <PureListView
          data={files}
          renderRow={this.renderFile}
          renderEmptyList={this.renderEmptyFileList}
          renderSeparator={true}
          alwaysBounceVertical={false}
          style={styles.listView}
          tabBarSpace={false}
        />
      </View>
    );
  }
}

function select(state) {
  return {
    favorites: _.orderBy(state.favorites.list, ({added_on}) => added_on || '', ['desc']),
    folderMap: state.folderMap
  };
}

module.exports = connect(select)(FolderView);
