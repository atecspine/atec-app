import React from 'react';
import { 
  StyleSheet,  
  Text,
  View,
  Image,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';
import FileImage from '../../common/FileImage';
import { Heading3 } from '../../common/AppText';
import firebase from 'react-native-firebase';
import {normalize} from '../../util/normalize';
import {connect} from 'react-redux';

import {
  addFileToHistory,
  toggleFavoriteFile,
} from '../../actions';

class FavoritesCell extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      thumbnail_url: null,
      file_url: null,
    };

    this.showFile = this.showFile.bind(this);
  }

  componentDidMount() {
    let {file_thumbnail_url, file_url, offline_path, offline_thumbnail_path} = this.props.file;
    
    if (offline_thumbnail_path) {
      let thumbnail_path = `${firebase.storage.Native.CACHES_DIRECTORY_PATH}/${file_thumbnail_url}`;
      this.setState({thumbnail_url: thumbnail_path});
    } else {
      const file_thumbnail_ref = firebase.storage().ref(file_thumbnail_url);
      file_thumbnail_ref.getDownloadURL().then((url) => {
        this.setState({thumbnail_url: url});
      });
    }
  
    if (offline_path) {
      let file_path = `${firebase.storage.Native.CACHES_DIRECTORY_PATH}/${file_url}`;
      this.setState({file_url: file_path});
    } else {
      const file_ref = firebase.storage().ref(file_url);
      file_ref.getDownloadURL().then((url) => {
        this.setState({file_url: url});
      });
    }
  }

  showFile() {
    if (this.state.file_url) {
      this.props.dispatch(addFileToHistory(this.props.file));
      this.props.navigator.push({
        screen: 'atec.FileView',
        title: this.props.file.file_type,
        backButtonTitle: '',
        passProps: {
          file: this.props.file,
          file_url: this.state.file_url,
        },
      });
    } else {
      alert('There was an issue downloading the file. Please check your connection and try again.');
    }
  }

  render() {
    let {file_name, file_url, offline_path} = this.props.file;
    const {selected} = this.props;

    let checkmark, downloadedIndicator;
    let check = selected
      ? <Image style={styles.check} source={require('../../common/img/checkmark_filled.png')} />
      : <Image style={styles.check} source={require('../../common/img/checkmark_empty.png')} />;
    checkmark = (
      <TouchableOpacity onPress={this.props.onSelect} style={styles.checkmark}>
        {check}
      </TouchableOpacity>
    );
    if (offline_path) {
      downloadedIndicator = (
        <View style={styles.downloadedIndicator}>
          <Image style={styles.indicator} source={require('../../common/img/downloaded.png')} />
          <Text style={styles.downloadText}>Downloaded</Text>
        </View>
      );
    }

    return (
      <TouchableHighlight onPress={this.showFile}>
        <View style={styles.cell}>
          {checkmark}
          <View>
            <FileImage file={this.props.file} thumbnail_url={this.state.thumbnail_url} />
            {downloadedIndicator}
          </View>
          <View style={styles.details}>
            <Heading3 style={styles.cellTitle}>{file_name}</Heading3>
            <View style={styles.actions}>
              <TouchableOpacity style={styles.toggleButton} onPress={() => this.props.dispatch(toggleFavoriteFile(this.props.file))}>
                <Image source={require('./img/heart-icon-solid-small.png')} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  cell: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingVertical: 15,
    paddingHorizontal: 25,
  },
  downloadedIndicator: {
    flexDirection: 'row',
  },
  downloadText: {
    fontFamily: 'Soleto',
    fontSize: normalize(15),
  },
  indicator: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  details: {
    flex: 1,
    paddingLeft: 15,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  cellTitle: {
    paddingBottom: 25,
  },
  toggleButton: {
    width: normalize(30),
    height: normalize(30),
  },
  checkmark: {
    width: normalize(30),
    height: normalize(30),
    marginRight: normalize(15),
  }
});

function select(state) {
  return {
    selectedFiles: state.sharing.selectedFiles,
  };
}

module.exports = connect(select)(FavoritesCell);
