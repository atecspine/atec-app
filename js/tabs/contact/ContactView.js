import React from 'react';
import { StyleSheet, ScrollView, Text, View, TouchableOpacity, Linking } from 'react-native';
import AppColors from '../../common/AppColors';
import { Heading1, Heading2 } from '../../common/AppText';
import firebase from 'react-native-firebase';
import {connect} from 'react-redux';
import {
  updateContact,
} from '../../actions';
import _ from 'lodash';

import AppBaseScreeen from '../../AppBaseScreen';

class ContactView extends AppBaseScreeen {
  static navigatorButtons = {
    rightButtons: [
      {
        id: 'search',
        systemItem: 'search'
      }
    ],
  }

  constructor(props) {
    super(props);

    this.contact_ref = firebase.firestore().collection('contact');
    this.state = {
      isLoading: true,
      showShadow: false,
    };

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.onContactUpdate = this.onContactUpdate.bind(this);
    this.openUrl = this.openUrl.bind(this);
    this._onScroll = this._onScroll.bind(this);
  }

  componentDidMount() {
    this.contactUnsubscribe = this.contact_ref.onSnapshot(this.onContactUpdate);
  }

  componentWillUnmount() {
    this.contactUnsubscribe();
  }

  onNavigatorEvent(event) {
    if (event.type == 'NavBarButtonPress') {
      if (event.id == 'search') {
        this.props.navigator.showModal({
          screen: 'atec.SearchView',
          title: 'Search',
          titleImage: require('../../common/img/header-logo.png'),
          passProps: {}
        });
      }
    }
  } 

  onContactUpdate = (querySnapshot) => {
    this.setState({isLoading: false});
    this.props.updateContact(querySnapshot);
  }

  openUrl(url) {
    Linking.openURL(url).catch(err => console.log('Error occured', err));
  }

  _onScroll(event) {
    var currentOffset = event.nativeEvent.contentOffset.y;
    if (currentOffset > 0) {
      this.setState({showShadow: true});
    } else {
      this.setState({showShadow: false});
    }
  }

  renderContent() {
    return (
      <View style={styles.container}>
        <View style={[styles.viewWrap, styles.shadow, this.state.showShadow ? {zIndex: 1} : {}]}>
          <Heading1 style={styles.headingText}>Contact</Heading1>
        </View>
        <ScrollView 
          onScroll={this._onScroll}
          contentContainerStyle={[styles.viewWrap, {paddingTop: 10}]} 
          alwaysBounceVertical={false}
          style={styles.listView}
        >
          <Heading2 style={styles.subHeadingText}>Sales & Customer Service</Heading2>
          <ContactRow label="P:" contact="+1.800.922.1356" extraContent="[Tollfree]" onPress={() => this.openUrl('tel:18009221356')} />
          <ContactRow label="P:" contact="+1.760.431.9286" onPress={() => this.openUrl('tel:17604319286')} />
          <ContactRow label="F:" contact="+1.800.432.9722" onPress={() => null} />
          <ContactRow label="E:" contact="customerservice@atecspine.com" onPress={() => this.openUrl('mailto:customerservice@atecspine.com')} />
          <View style={styles.section}>
            <View><Text style={[styles.text, styles.label]}>Hours of Operation:</Text></View>
            <View><Text style={[styles.text, styles.contactText]}>Mon-Fri 6am&mdash;5:30pm PST</Text></View>
          </View>
          <View style={styles.section}>
            <View><Text style={[styles.text, styles.label]}>Address:</Text></View>
            <TouchableOpacity activeOpacity={0.8} onPress={() => this.openUrl(`http://maps.apple.com/?q=${encodeURIComponent('5818 El Camino Real Carlsbad, CA 92008 USA')}`)}>
              <Text style={[styles.text, styles.contactText]} selectable={true}>{`ATEC Spine\n5818 El Camino Real\nCarlsbad, CA 92008 USA`}</Text>
            </TouchableOpacity>
          </View>
          {this.props.contacts.map(contact => {
            return (
              <ManagerContactRow key={contact.email} contact={contact} />
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

class ManagerContactRow extends React.Component {

  openUrl(url) {
    Linking.openURL(url).catch(err => console.log('Error occured', err));
  }

  render() {
    let {
      name, title, email, phone, mobile,
    } = this.props.contact;
    return (
      <View style={styles.section}>
        <Text style={[styles.text, styles.label]}>{name}</Text>
        {title && <Text style={[styles.text, styles.contactText]}>{title}</Text>}
        {email && <ContactRow label="E:" contact={email} onPress={() => this.openUrl(`mailto:${email}`)} />}
        {phone && <ContactRow label="P:" contact={phone} onPress={() => this.openUrl(`tel:${phone}`)} />}
        {mobile && <ContactRow label="M:" contact={mobile} onPress={() => this.openUrl(`tel:${mobile}`)} />}
      </View>
    );
  }
}

class ContactRow extends React.Component {

  render() {
    let extra;
    if (this.props.extraContent) {
      extra = <Text style={[styles.text, styles.contactText, styles.extraContent]}>{this.props.extraContent}</Text>;
    }
    return (
      <View style={styles.contactRow}>
        <Text style={[styles.text, styles.label]}>{this.props.label}</Text>
        <TouchableOpacity activeOpacity={0.8} onPress={this.props.onPress}>
          <Text style={[styles.text, styles.contactText]} selectable={true}>{this.props.contact}</Text>
        </TouchableOpacity>
        {extra}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppColors.white,
    borderColor: AppColors.brightGreen,
    borderTopWidth: 2,
  },
  viewWrap: {
    padding: 25,
  },
  listView: {
    flex: 1,
    backgroundColor: 'white',
  },
  subHeadingText: {
    marginBottom: 20,
  },
  section: {
    marginTop: 20,
  },
  text: {
    fontFamily: 'Soleto',
    color: AppColors.darkBlue,
    fontSize: 18,
  },
  label: {
    fontWeight: '500',
    marginRight: 5,
  },
  contactRow: {
    flexDirection: 'row',
  },
  contactText: {
    fontWeight: '300',
  },
  extraContent: {
    paddingLeft: 5,
  },
  shadow: {
    shadowColor: 'rgba(0, 0, 0, 0.2)',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 4},
    shadowRadius: 4,
  },
});

function actions(dispatch) {
  return {
    updateContact: (querySnapshot) => dispatch(updateContact(querySnapshot)),
  };
}

function select(state) {
  return {
    contacts: _.sortBy(state.contact.list, 'order'),
  };
}

module.exports = connect(select, actions)(ContactView);
