import React from 'react';
import { StyleSheet, ScrollView, View } from 'react-native';
import AppColors from '../../common/AppColors';
import { Heading1 } from '../../common/AppText';
import NewsletterCell from './NewsletterCell';
import PureListView from '../../common/PureListView';
import firebase from 'react-native-firebase';
import {connect} from 'react-redux';
import {
  updateNewsletters,
} from '../../actions';
import _ from 'lodash';

import AppBaseScreeen from '../../AppBaseScreen';

class NewslettersView extends AppBaseScreeen {
  static navigatorButtons = {
    rightButtons: [
      {
        id: 'search',
        systemItem: 'search'
      }
    ],
  }

  constructor(props) {
    super(props);

    this.newsletters_ref = firebase.firestore().collection('newsletters');
    this.state = {
      isLoading: true,
      showShadow: false,
    };

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.onNewslettersUpdate = this.onNewslettersUpdate.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this._onScroll = this._onScroll.bind(this);
  }

  componentDidMount() {
    this.newslettersUnsubscribe = this.newsletters_ref.onSnapshot(this.onNewslettersUpdate);
  }

  componentWillUnmount() {
    this.newslettersUnsubscribe();
  }

  onNavigatorEvent(event) {
    if (event.type == 'NavBarButtonPress') {
      if (event.id == 'search') {
        this.props.navigator.showModal({
          screen: 'atec.SearchView',
          title: 'Search',
          titleImage: require('../../common/img/header-logo.png'),
          passProps: {}
        });
      }
    }
  }  

  onNewslettersUpdate = (querySnapshot) => {
    this.setState({isLoading: false});
    this.props.updateNewsletters(querySnapshot);
  }

  renderRow(n, idx) {
    // Look to see if this file has been saved, if it has
    // use the saved file instead (uses offline storage)
    let savedFile = _.find(this.props.savedFiles, ['file_url', n.file_url]);
    if (savedFile) {
      n = savedFile;
    }
  
    return (
      <NewsletterCell
        key={idx}
        navigator={this.props.navigator}
        newsletter={n}
      />
    );
  }

  _onScroll(event) {
    var currentOffset = event.nativeEvent.contentOffset.y;
    if (currentOffset > 0) {
      this.setState({showShadow: true});
    } else {
      this.setState({showShadow: false});
    }
  }

  renderContent() {
    return (
      <View style={styles.container}>
        <View style={[styles.viewWrap, styles.shadow, this.state.showShadow ? {zIndex: 1} : {}]}>
          <Heading1 style={styles.headingText}>Frontline</Heading1>
        </View>
        <PureListView
          alwaysBounceVertical={false}
          data={this.props.data}
          renderRow={this.renderRow}
          style={styles.listView}
          renderSeparator={true}
          tabBarSpace={false}
          onScroll={this._onScroll}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppColors.white,
    borderColor: AppColors.brightGreen,
    borderTopWidth: 2,
  },
  viewWrap: {
    padding: 25,
    paddingBottom: 15,
  },
  listView: {
    flex: 1,
    backgroundColor: 'white',
  },
  subHeadingText: {
    marginBottom: 20,
  },
  section: {
    marginTop: 20,
  },
  text: {
    fontFamily: 'Soleto',
    color: AppColors.darkBlue,
    fontSize: 18,
  },
  label: {
    fontWeight: '500',
    marginRight: 5,
  },
  extraContent: {
    paddingLeft: 5,
  },
  shadow: {
    shadowColor: 'rgba(0, 0, 0, 0.2)',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 4},
    shadowRadius: 4,
  },
});

function actions(dispatch) {
  return {
    updateNewsletters: (querySnapshot) => dispatch(updateNewsletters(querySnapshot)),
  };
}

function select(state) {
  return {
    data: _.sortBy(state.newsletters.list, 'order'),
    savedFiles: state.favorites.list,
  };
}

module.exports = connect(select, actions)(NewslettersView);
