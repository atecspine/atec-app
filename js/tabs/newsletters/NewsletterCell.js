import React from 'react';
import { StyleSheet, TouchableHighlight, View } from 'react-native';
import { Heading2 } from '../../common/AppText';
import FileImage from '../../common/FileImage';
import firebase from 'react-native-firebase';
import {connect} from 'react-redux';
import {
  addFileToHistory,
} from '../../actions';

class NewsletterCell extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      thumbnail_url: null,
      file_url: null,
    };

    this.showFile = this.showFile.bind(this);
  }

  componentWillMount() {
    let {file_thumbnail_url, file_url, offline_path, offline_thumbnail_path} = this.props.newsletter;
    
    if (offline_thumbnail_path) {
      let thumbnail_path = `${firebase.storage.Native.CACHES_DIRECTORY_PATH}/${file_thumbnail_url}`;
      this.setState({thumbnail_url: thumbnail_path});
    } else {
      const file_thumbnail_ref = firebase.storage().ref(file_thumbnail_url);
      file_thumbnail_ref.getDownloadURL().then((url) => {
        this.setState({thumbnail_url: url});
      });
    }
  
    if (offline_path) {
      let file_path = `${firebase.storage.Native.CACHES_DIRECTORY_PATH}/${file_url}`;
      this.setState({file_url: file_path});
    } else {
      const file_ref = firebase.storage().ref(file_url);
      file_ref.getDownloadURL().then((url) => {
        this.setState({file_url: url});
      });
    }
  }

  showFile() {
    if (this.state.file_url) {
      this.props.onShowFile(this.props.newsletter);
      this.props.navigator.push({
        screen: 'atec.FileView',
        title: this.props.newsletter.file_type,
        backButtonTitle: '',
        passProps: {
          file: this.props.newsletter,
          file_url: this.state.file_url,
        },
      });
    } else {
      alert('There was an issue downloading the file. Please check your connection and try again.');
    }
  }

  render() {
    let {file_name} = this.props.newsletter;
    return (
      <TouchableHighlight onPress={this.showFile}>
        <View style={styles.cell}>
          <FileImage file={this.props.newsletter} thumbnail_url={this.state.thumbnail_url} />
          <Heading2 style={styles.cellTitle}>{file_name}</Heading2>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  cell: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingHorizontal: 25,
  },
  cellTitle: {
    flex: 1,
    paddingLeft: 15,
  }
});

function actions(dispatch) {
  return {
    onShowFile: (file) => dispatch(addFileToHistory(file)),
  };
}

module.exports = connect(null, actions)(NewsletterCell);