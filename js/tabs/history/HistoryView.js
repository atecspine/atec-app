import React from 'react';
import { StyleSheet, View } from 'react-native';
import AppColors from '../../common/AppColors';
import { Heading1, Heading2} from '../../common/AppText';
import PureListView from '../../common/PureListView';
import HistoryRow from './HistoryRow';
import {connect} from 'react-redux';
import {groupHistory} from '../../util/history';
import _ from 'lodash';

import AppBaseScreeen from '../../AppBaseScreen';

class HistoryView extends AppBaseScreeen {
  static navigatorButtons = {
    rightButtons: [
      {
        id: 'search',
        systemItem: 'search'
      }
    ],
  }
  
  constructor(props) {
    super(props);

    this.state = {
      showShadow: false,
    }

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.renderRow = this.renderRow.bind(this);
    this.renderSectionHeader = this.renderSectionHeader.bind(this);
    this._onScroll = this._onScroll.bind(this);
  }

  onNavigatorEvent(event) {
    if (event.type == 'NavBarButtonPress') {
      if (event.id == 'search') {
        this.props.navigator.showModal({
          screen: 'atec.SearchView',
          title: 'Search',
          titleImage: require('../../common/img/header-logo.png'),
          passProps: {}
        });
      }
    }
  }

  renderSectionHeader(sectionData, sectionID) {
    return (
      <View style={[styles.sectionHeader]}>
        <Heading2 style={styles.headingText}>{sectionID}</Heading2>
      </View>
    );
  }

  renderRow(file, sectionID, rowID) {
    // Look to see if this file has been saved, if it has
    // use the saved file instead (uses offline storage)
    // If it is not saved, make sure to remove the offline paths if they exist
    // otherwise it will cause issues
    let savedFile = _.find(this.props.savedFiles, ['file_url', file.file_url]);
    if (savedFile) {
      file = savedFile;
    } else {
      delete file.offline_path;
      delete file.offline_thumbnail_path;
    }

    return (
      <HistoryRow
        key={rowID}
        file={file}
        navigator={this.props.navigator}
      />
    );
  }

  _onScroll(event) {
    var currentOffset = event.nativeEvent.contentOffset.y;
    if (currentOffset > 0) {
      this.setState({showShadow: true});
    } else {
      this.setState({showShadow: false});
    }
  }

  renderContent() {
    return (
      <View style={styles.container}>
        <View style={[styles.viewWrap]}>
          <Heading1 style={styles.headingText}>History</Heading1>
        </View>
        <PureListView
          data={this.props.data}
          renderRow={this.renderRow}
          renderSectionHeader={this.renderSectionHeader}
          style={styles.listView}
          tabBarSpace={false}
          onScroll={this._onScroll}
          alwaysBounceVertical={false}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppColors.white,
    borderColor: AppColors.brightGreen,
    borderTopWidth: 2,
  },
  listView: {
    paddingVertical: 15,
    paddingTop: 0,
    backgroundColor: AppColors.white,
  },
  productsFeature: {
    height: 150,
    maxHeight: '35%',
    backgroundColor: AppColors.darkBlue,
    elevation: 1,
    zIndex: 3,
  },
  viewWrap: {
    padding: 25,
  },
  sectionHeader: {
    paddingVertical: 5,
    paddingHorizontal: 30,
    backgroundColor: AppColors.white,
  },
  shadow: {
    shadowColor: 'rgba(0, 0, 0, 0.2)',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 4},
    shadowRadius: 4,
  },
});

function select(state) {
  return {
    savedFiles: state.favorites.list,
    data: groupHistory(state.history.list),
  };
}

module.exports = connect(select)(HistoryView);
