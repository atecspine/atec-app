import React from 'react';
import { 
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import AppColors from '../../common/AppColors';
import firebase from 'react-native-firebase';
import {connect} from 'react-redux';

import {
  addFileToHistory,
} from '../../actions';

class HistoryRow extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      file_url: null,
    };

    this.showFile = this.showFile.bind(this);
  }

  componentDidMount() {
    let {file_url, offline_path} = this.props.file;
  
    if (offline_path) {
      let file_path = `${firebase.storage.Native.CACHES_DIRECTORY_PATH}/${file_url}`;
      this.setState({file_url: file_path});
    } else {
      const file_ref = firebase.storage().ref(file_url);
      file_ref.getDownloadURL().then((url) => {
        this.setState({file_url: url});
      });
    }
  }

  showFile() {
    if (this.state.file_url) {
      this.props.dispatch(addFileToHistory(this.props.file));
      this.props.navigator.push({
        screen: 'atec.FileView',
        title: this.props.file.file_type,
        backButtonTitle: '',
        passProps: {
          file: this.props.file,
          file_url: this.state.file_url,
        },
      });
    } else {
      alert('There was an issue downloading the file. Please check your connection and try again.');
    }
  }

  render() {
    let {file_name} = this.props.file;
    return (
      <View style={styles.content}>
        <TouchableOpacity onPress={this.showFile}><Text style={styles.fileName}>{file_name}</Text></TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    flexDirection: 'column',
    paddingVertical: 5,
    paddingHorizontal: 30,
  },
  fileName: {
    justifyContent: 'center',
    fontFamily: 'Soleto',
    color: AppColors.darkBlue,
    padding: 10,
    fontSize: 16,
  },
});

module.exports = connect()(HistoryRow);
