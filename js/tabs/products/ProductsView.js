import React from 'react';
import { 
  StyleSheet, 
  View,
} from 'react-native';
import AppColors from '../../common/AppColors';
import PureListView from '../../common/PureListView';
import ProductCell from './ProductCell';
import { Heading1 } from '../../common/AppText';
import _ from 'lodash';

import AppBaseScreeen from '../../AppBaseScreen';

class ProductsView extends AppBaseScreeen {
  static navigatorButtons = {
    rightButtons: [
      {
        id: 'search',
        systemItem: 'search'
      }
    ]
  };

  constructor(props) {
    super(props);

    this.state = {
      showShadow: false,
    }

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.showDetails = this.showDetails.bind(this);
    this._onScroll = this._onScroll.bind(this);
    this.renderRow = this.renderRow.bind(this);
  }

  onNavigatorEvent(event) {
    if (event.type == 'NavBarButtonPress') {
      if (event.id == 'search') {
        this.props.navigator.showModal({
          screen: 'atec.SearchView',
          title: 'Search',
          titleImage: require('../../common/img/header-logo.png'),
          passProps: {}
        });
      }
    }
  }

  showDetails(product) {
    this.props.navigator.push({
      screen: 'atec.ProductDetailsView',
      title: product.name,
      titleImage: require('../../common/img/header-logo.png'),
      backButtonTitle: '',
      passProps: {
        product: product,
      }
    });
  }

  _onScroll(event) {
    var currentOffset = event.nativeEvent.contentOffset.y;
    if (currentOffset > 0) {
      this.setState({showShadow: true});
    } else {
      this.setState({showShadow: false});
    }
  }

  renderRow(p) {
    let { name } = p;
    let { icon_url } = p;
    return (
      <ProductCell
        key={name}
        title={name}
        thumbnail={icon_url}
        onPress={() => this.showDetails(p)}
      />
    );
  }

  renderContent() {
    let { category, products_list } = this.props.products;

    let data = _.sortBy(Object.values(products_list), 'order');
    return (
      <View style={styles.container}>
        <View style={[styles.viewWrap, styles.shadow, this.state.showShadow ? {zIndex: 1} : {}]}>
          <Heading1 style={styles.headingText}>{category}</Heading1>
        </View>
        <PureListView
          data={data}
          renderRow={this.renderRow}
          style={styles.listView}
          renderSeparator={true}
          tabBarSpace={false}
          onScroll={this._onScroll}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppColors.white,
    borderColor: AppColors.brightGreen,
    borderTopWidth: 2,
  },
  viewWrap: {
    paddingVertical: 15,
    paddingHorizontal: 25,
  },
  shadow: {
    shadowColor: 'rgba(0, 0, 0, 0.2)',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 4},
    shadowRadius: 4,
  },
});

module.exports = ProductsView;
