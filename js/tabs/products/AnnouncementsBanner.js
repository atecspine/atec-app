import React from 'react';
import { 
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  Linking,
  Platform,
} from 'react-native';
import {CachedImage} from 'react-native-cached-image';
import AppColors from '../../common/AppColors';
import {normalize} from '../../util/normalize';
import SafariView from 'react-native-safari-view';
import {
  updateAnnouncements,
} from '../../actions';
import firebase from 'react-native-firebase';
import {connect} from 'react-redux';
import _ from 'lodash';

class AnnouncementsBanner extends React.Component {
  constructor(props) {
    super(props);

    this.announcements_ref = firebase.firestore().collection('announcements');

    this.onAnnouncementsUpdate = this.onAnnouncementsUpdate.bind(this);
    this.showAnnouncement = this.showAnnouncement.bind(this);
  }

  componentDidMount() {
    this.announcementsUnsubscribe = this.announcements_ref.onSnapshot(this.onAnnouncementsUpdate);
  }

  componentWillUnmount() {
    this.announcementsUnsubscribe();
  }

  onAnnouncementsUpdate = (querySnapshot) => {
    this.props.updateAnnouncements(querySnapshot);
  }

  showAnnouncement(url) {
    if (Platform.OS === 'ios') {
      SafariView.isAvailable()
      .then(SafariView.show({
        url: url,
        barTintColor: AppColors.darkBlue,
        tintColor: AppColors.brightGreen,
      }))
      .catch(error => {
        Linking.openURL(url).catch(err => console.error('An error occurred', err));
      });
    } else {
      Linking.openURL(url).catch(err => console.error('An error occurred', err));
    }
  }

  render() {
    return (
      <View style={[styles.productsFeature, styles.shadow]}>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          alwaysBounceHorizontal={false}
          horizontal={true}
        >
          {this.props.announcements.map((announcement, idx) => {
            return (
              <Announcement
                key={idx}
                onPress={() => this.showAnnouncement(announcement.url)}
                image={announcement.image_url}
              />
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

class Announcement extends React.Component {
  render() {
    return (
      <TouchableOpacity activeOpacity={0.5} style={styles.announcement} onPress={this.props.onPress}>
        <CachedImage style={styles.announcementImage} source={{uri: this.props.image}}/>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  productsFeature: {
    backgroundColor: AppColors.darkBlue,
    elevation: 1,
    zIndex: 3,
    paddingVertical: 15,
    justifyContent: "center",
    alignItems: "center",
  },
  announcement: {
    margin: 15,
  },
  announcementImage: {
    width: normalize(300),
    height: normalize(131),
    borderWidth: 1,
    borderColor: AppColors.brightGreen,
    borderRadius: 10,
  },
  shadow: {
    shadowColor: 'rgba(0, 0, 0, 0.2)',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 4},
    shadowRadius: 4,
  },
});

function actions(dispatch) {
  return {
    updateAnnouncements: (querySnapshot) => dispatch(updateAnnouncements(querySnapshot)),
  };
}

function select(state) {
  return {
    announcements: _.sortBy(state.announcements.list, 'order'),
  };
}

module.exports = connect(select, actions)(AnnouncementsBanner);
