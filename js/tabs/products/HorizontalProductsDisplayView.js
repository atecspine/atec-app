import React from 'react';
import { 
  StyleSheet, 
  View,
  ScrollView,
} from 'react-native';
import { Heading1 } from '../../common/AppText';
import { normalize } from '../../util/normalize';
import FileCell from './FileCell';
import {connect} from 'react-redux';
import _ from 'lodash';

class HorizontalProductsDisplayView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showShadow: false,
    }

    this.renderFile = this.renderFile.bind(this);
  }

  renderFile(file, idx) {
    // Look to see if this file has been saved, if it has
    // use the saved file instead (uses offline storage)
    let savedFile = _.find(this.props.savedFiles, ['file_url', file.file_url]);
    if (savedFile) {
      file = savedFile;
    }

    return <FileCell key={idx} file={file} navigator={this.props.navigator} />;
  }

  render() {
    let files = this.props.content;
    return (
      <View style={styles.container}>
        <View style={styles.headerWrap}>
          <Heading1 style={styles.headingText}>{this.props.title.toUpperCase()}</Heading1>
        </View>
        <ScrollView  
          showsHorizontalScrollIndicator={false}
          alwaysBounceHorizontal={false}
          horizontal={true}
          contentContainerStyle={styles.scrollView}
        >
          {files.map(this.renderFile)}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
  },
  headerWrap: {
    paddingTop: 10,
    paddingLeft: 25,
  },
  headingText: {
    fontWeight: '100',
    fontSize: normalize(26),
  },
  scrollView: {
    paddingHorizontal: 25,
  }
});

function select(state) {
  return {
    savedFiles: state.favorites.list,
  };
}

module.exports = connect(select)(HorizontalProductsDisplayView);
