import React from 'react';
import { 
  StyleSheet, 
  Image, 
  WebView,
  View,
  TouchableOpacity,
  ActivityIndicator,
  Share,
} from 'react-native';
import AppColors from '../../common/AppColors';
import EmptyList from '../../common/EmptyList';
import {connect} from 'react-redux';
import Video from 'react-native-video';
import firebase from 'react-native-firebase';

import {
  toggleFavoriteFile
} from '../../actions';

import AppBaseScreeen from '../../AppBaseScreen';

let WEBVIEW_REF = 'webview';

class FileView extends AppBaseScreeen {
  static navigatorStyle = {
    tabBarHidden: true,
    drawUnderTabBar: true,
  };

  constructor(props) {
    super(props);

    this.state = {
      isVisible: false,
      paused: true,
      loading: true,
    };

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.toggleFavorite = this.toggleFavorite.bind(this);
    this.renderError = this.renderError.bind(this);
    this.shareFile = this.shareFile.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.isVisible;
  }

  onNavigatorEvent(event) {
    switch (event.id) {
      case 'save':
        this.toggleFavorite();
        break;
      case 'share':
        this.shareFile();
        break;
      case 'willAppear':
        this.setState({isVisible: true});
        break;
      case 'willDisappear':
        this.setState({isVisible: false});
        break;        
    }
  }

  shareFile() {
    Share.share({
      message: 'I want to share a file I found on the ATECspine app',
      title: 'Checkout this file',
      url: `https://firebasestorage.googleapis.com/v0/b/atec-spine-dev.appspot.com/o/${encodeURIComponent(this.props.file.file_url)}?alt=media`
    });
  }

  toggleFavorite() {
    this.props.dispatch(toggleFavoriteFile(this.props.file));
  }

  renderError(errorDomain, errorCode, errorDesc) {
    return (
      <View style={{flex: 1}}>
        <EmptyList
          title="Error loading file"
          text={errorDesc}
        />
      </View>
    );
  }

  renderContent() {
    let favoriteButton;
    if (this.props.favorites.some(f => f.file_url === this.props.file.file_url)) {
      favoriteButton = {
        icon: require('../favorites/img/heart-icon-solid-small.png'),
        id: 'save',
        systemItem: 'add'
      }
    } else {
      favoriteButton = {
        icon: require('../favorites/img/heart-icon-outline-small.png'),
        id: 'save',
        systemItem: 'save',
      }
    }
    this.props.navigator.setButtons({
      rightButtons: [
        favoriteButton,
        {
          icon: require('../../common/img/share.png'),
          id: 'share',
        }
      ],
    });

    let content, playButton, file_url;
    if (this.props.file.offline_path) {
      let file_path = `${firebase.storage.Native.CACHES_DIRECTORY_PATH}/${this.props.file.file_url}`;
      file_url = file_path;
    } else {
      file_url = this.props.file_url;
    }
    if (this.props.file.file_type === 'Video') {
      if (!this.state.loading) {
        playButton = (
          <View
            style={styles.playButton}
          >
            <Image source={require('../../common/img/playButton.png')} />
          </View>
        );
      }
      content = (
        <TouchableOpacity 
          activeOpacity={1.0} 
          style={styles.videoWrap} 
          onPress={() => {
            this.player.presentFullscreenPlayer();
          }}>
          <Video 
            ref={(ref) => {
              this.player = ref;
            }}
            resizeMode="scale"
            source={{uri: file_url}}
            style={styles.video}
            paused={this.state.paused}
            onLoad={() => this.setState({loading: false})}
            onFullscreenPlayerWillPresent={() => this.setState({paused: false})}
            onFullscreenPlayerWillDismiss={() => this.setState({paused: true})}
          />
          {playButton}
        </TouchableOpacity>
      );
    } else {
      content = (
        <WebView
          ref={WEBVIEW_REF}
          source={{uri: file_url}}
          style={styles.webView}
          renderError={this.renderError}
          onError={() => this.setState({loading: false})}
          onLoad={() => this.setState({loading: false})}
          scalesPageToFit={true}
          automaticallyAdjustContentInsets={true}
          allowsInlineMediaPlayback={false}
        />
      );
    }

    return (
      <View style={styles.container}>
        {content}
        {this.state.loading && (
          <ActivityIndicator
            style={styles.loadingContainer}
            size="large"
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppColors.tableViewBackground,
    borderColor: AppColors.brightGreen,
    borderTopWidth: 2,
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  playButton: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  videoWrap: {
    flex: 1,
  },
  webView: {
    flex: 1,
    backgroundColor: AppColors.tableViewBackground,
  },
  video: {
    flex: 1,
  }
});

function select(state) {
  return {
    user: state.user,
    favorites: state.favorites.list,
  };
}

module.exports = connect(select)(FileView);
