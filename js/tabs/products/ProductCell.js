import React from 'react';
import { 
  StyleSheet, 
  View,
  TouchableHighlight,
} from 'react-native';
import {CachedImage} from 'react-native-cached-image';
import AppColors from '../../common/AppColors';
import { Heading2 } from '../../common/AppText';
import {normalize} from '../../util/normalize';

class ProductCell extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableHighlight onPress={this.props.onPress}>
        <View style={styles.cell}>
          <CachedImage style={styles.cellImage} source={{uri: this.props.thumbnail}}/>
          <Heading2 style={styles.cellTitle}>{this.props.title}</Heading2>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  cell: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingVertical: 15,
    paddingHorizontal: 25,
  },
  cellImage: {
    width: normalize(150),
    height: normalize(150),
    borderWidth: 1,
    borderColor: AppColors.brightGreen,
    borderRadius: 6,
    backgroundColor: AppColors.lighterGray,
  },
  cellTitle: {
    flex: 1,
    paddingLeft: 15,
  }
});

module.exports = ProductCell;
