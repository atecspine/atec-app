import React from 'react';
import { 
  StyleSheet,  
  View,
  ScrollView,
} from 'react-native';
import AppColors from '../../common/AppColors';
import { Heading1 } from '../../common/AppText';
import HorizontalProductsDisplayView from './HorizontalProductsDisplayView';
import ItemsWithSeparator from '../../common/ItemsWithSeparator';
import _ from 'lodash';

import AppBaseScreeen from '../../AppBaseScreen';

class ProductDetailsView extends AppBaseScreeen {
  static navigatorButtons = {
    rightButtons: [
      {
        id: 'search',
        systemItem: 'search'
      }
    ]
  };

  constructor(props) {
    super(props);

    this.state = {
      showShadow: false,
    }

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.renderVideo = this.renderVideo.bind(this);
    this.renderSales = this.renderSales.bind(this);
    this.renderTechnical = this.renderTechnical.bind(this);
  }

  onNavigatorEvent(event) {
    if (event.type == 'NavBarButtonPress') {
      if (event.id == 'search') {
        this.props.navigator.showModal({
          screen: 'atec.SearchView',
          title: 'Search',
          titleImage: require('../../common/img/header-logo.png'),
          passProps: {}
        });
      }
    }
  }

  renderVideo() {
    if (!this.props.product.video_files) {
      return;
    }

    return (
      <HorizontalProductsDisplayView
        title="Videos"
        content={_.sortBy(this.props.product.video_files, 'order')}
        navigator={this.props.navigator}
      />
    )
  }

  renderSales() {
    if (!this.props.product.sales_files) {
      return;
    }

    return (
      <HorizontalProductsDisplayView
        title="Sales"
        content={_.sortBy(this.props.product.sales_files, 'order')}
        navigator={this.props.navigator}
      />
    )
  }

  renderTechnical() {
    if (!this.props.product.technical_files) {
      return;
    }

    return (
      <HorizontalProductsDisplayView
        title="Technical"
        content={_.sortBy(this.props.product.technical_files, 'order')}
        navigator={this.props.navigator}
      />
    )
  }

  renderAdmin() {
    if (!this.props.product.admin_files) {
      return;
    }

    return (
      <HorizontalProductsDisplayView
        title="Administrative"
        content={_.sortBy(this.props.product.admin_files, 'order')}
        navigator={this.props.navigator}
      />
    )
  }

  renderContent() {
    return (
      <View style={styles.container}>
        <View style={styles.headerWrap}>
          <Heading1 style={styles.headingText}>{this.props.product.name}</Heading1>
        </View>
        <View style={styles.separator} />
        <ScrollView  
          alwaysBounceVertical={false}
          contentContainerStyle={styles.scrollView}
        >
          <ItemsWithSeparator style={styles.contentWrap} separatorStyle={styles.separator}>
            {this.renderVideo()}
            {this.renderTechnical()}
            {this.renderSales()}
            {this.renderAdmin()}
          </ItemsWithSeparator>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppColors.white,
    borderColor: AppColors.brightGreen,
    borderTopWidth: 2,
  },
  headerWrap: {
    paddingVertical: 15,
    paddingLeft: 25,
    paddingBottom: 0,
  },
  headingText: {
    paddingBottom: 10,
  },
  contentWrap: {
    flex: 1,
    flexDirection: 'column',
  },
  separator: {
    backgroundColor: AppColors.cellBorder,
    height: 1,
    marginLeft: 25,
    marginBottom: 0,
  },
});

module.exports = ProductDetailsView;
