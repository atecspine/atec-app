import React from 'react';
import {
  StyleSheet,
  View,
  Linking,
  Platform,
  TouchableOpacity,
} from 'react-native';
import AppColors from '../../common/AppColors';
import PureListView from '../../common/PureListView';
import OrderCell from './OrderCell';
import DeviceInfo from 'react-native-device-info';
import firebase from 'react-native-firebase';
import { Heading1 } from '../../common/AppText';
import ProductCell from './ProductCell';
import AnnouncementsBanner from './AnnouncementsBanner';
import SafariView from 'react-native-safari-view';
import _ from 'lodash';
import {connect} from 'react-redux';
import {
  logOut,
  updateProducts,
  logOutWithPrompt,
} from '../../actions';

const ORDERING_TABS = [{
  thumbnail: require('./img/surgery-requisition-form.png'),
  title: 'Surgery Requisition Form',
  url: 'https://atecspine.com/atec-spine-srf/',
}, {
  thumbnail: require('./img/surgery-order-form.png'),
  title: 'Surgery Order Form',
  url: 'https://atecspine.com/atec-spine-sof/',
}];

import AppBaseScreeen from '../../AppBaseScreen';

class HomeView extends AppBaseScreeen {
  static navigatorButtons = {
    rightButtons: [
      {
        id: 'search',
        systemItem: 'search'
      }
    ],
    leftButtons: [
      {
        title: 'Log Out',
        id: 'logout',
      },
    ]
  };

  constructor(props) {
    super(props);

    this.FCM = firebase.messaging();
    this.products_ref = firebase.firestore().collection('products-v3');
    this.unsubscribe = null;

    this.state = {
      isLoading: true,
      showShadow: false,
      activeList: 'products',
    };

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.onProductsUpdate = this.onProductsUpdate.bind(this);
    this.onAuthChange = this.onAuthChange.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
    this._onScroll = this._onScroll.bind(this);
  }

  onNavigatorEvent(event) {
    if (event.type == 'NavBarButtonPress') {
      if (event.id == 'logout') {
        this.props.logOut();
      }
      if (event.id == 'search') {
        this.props.navigator.showModal({
          screen: 'atec.SearchView',
          title: 'Search',
          titleImage: require('../../common/img/header-logo.png'),
          passProps: {}
        });
      }
    }
  }

  componentDidMount() {
    this.authUnsubscribe = firebase.auth().onAuthStateChanged(this.onAuthChange);
    this.productsUnsubscribe = this.products_ref.onSnapshot(this.onProductsUpdate);
  }

  componentWillUnmount() {
    this.authUnsubscribe();
    this.productsUnsubscribe();
  }

  onAuthChange = (user) => {
    if (user) {
      // force refresh to check if account is still active
      firebase.auth().currentUser.reload();

      // Set up user and push notifications
      this.FCM.requestPermission();
      this.userTopic = `${user.uid}`;
      this.FCM.subscribeToTopic(this.userTopic);
      return;
    } else if (this.userTopic) {
      this.FCM.unsubscribeFromTopic(this.userTopic);
    }
    this.props.forceLogOut();
  }

  renderRow(p, idx) {
    let image, title, onPress;
    if (this.state.activeList === 'ordering') {
      image = p.thumbnail;
      title = p.title;
      onPress = () => this.showOrderingForm(p.url);

      return (
        <OrderCell
          key={idx}
          title={title}
          thumbnail={image}
          onPress={onPress}
        />
      );
    } else {
      image = p.icon_url;
      title = p.category;
      onPress = () => this.showProducts(p);

      return (
        <ProductCell
          key={idx}
          title={title}
          thumbnail={image}
          onPress={onPress}
        />
      );
    }
  }

  renderHeader() {
    let {activeList, showShadow} = this.state;
    return (
      <View>
        <AnnouncementsBanner />
        <View style={[styles.heading, showShadow ? styles.shadow : {}]}>
          <TouchableOpacity style={[styles.headingItem, activeList === 'products' ? styles.activeHeadingText : null]} onPress={() => this.setState({activeList: 'products'})}>
            <Heading1>Products</Heading1>
          </TouchableOpacity>
          <View style={styles.divider} />
          <TouchableOpacity style={[styles.headingItem, activeList === 'ordering' ? styles.activeHeadingText : null]} onPress={() => this.setState({activeList: 'ordering'})}>
            <Heading1>Order</Heading1>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  onProductsUpdate = (querySnapshot) => {
    this.setState({isLoading: false});
    this.props.updateProducts(querySnapshot);
  }

  showOrderingForm(url) {
    if (Platform.OS === 'ios') {
      SafariView.isAvailable()
      .then(SafariView.show({
        url: url,
        barTintColor: AppColors.darkBlue,
        tintColor: AppColors.brightGreen,
      }))
      .catch(error => {
        Linking.openURL(url).catch(err => console.error('An error occurred', err));
      });
    } else {
      Linking.openURL(url).catch(err => console.error('An error occurred', err));
    }
  }

  showProducts(p) {
    this.props.navigator.push({
      screen: 'atec.ProductsView',
      title: p.category,
      titleImage: require('../../common/img/header-logo.png'),
      backButtonTitle: '',
      passProps: {
        products: p
      }
    });
  }

  _onScroll(event) {
    var currentOffset = event.nativeEvent.contentOffset.y;
    if (currentOffset > 0) {
      this.setState({showShadow: true});
    } else {
      this.setState({showShadow: false});
    }
  }

  renderContent() {
    const data = this.state.activeList === 'products' ? this.props.productsData : ORDERING_TABS
    return (
      <View style={styles.container}>
        <PureListView
          stickyHeaderIndices={DeviceInfo.isTablet() ? [0] : []}
          alwaysBounceVertical={false}
          data={data}
          renderHeader={this.renderHeader}
          renderRow={this.renderRow}
          style={styles.listView}
          renderSeparator={true}
          tabBarSpace={false}
          onScroll={this._onScroll}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: AppColors.white,
    borderColor: AppColors.brightGreen,
    borderTopWidth: 2,
  },
  heading: {
    backgroundColor: AppColors.white,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: 15,
  },
  headingItem: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    marginHorizontal: 15,
  },
  divider: {
    width: 1,
    height: 20,
    backgroundColor: AppColors.darkBlue,
  },
  activeHeadingText: {
    borderBottomWidth: 1,
    borderColor: AppColors.brightGreen,
  },
  viewWrap: {
    paddingVertical: 15,
    paddingHorizontal: 25,
  },
  shadow: {
    shadowColor: 'rgba(0, 0, 0, 0.2)',
    shadowOpacity: 1,
    shadowOffset: {width: 0, height: 4},
    shadowRadius: 4,
  },
});

function actions(dispatch) {
  return {
    logOut: () => dispatch(logOutWithPrompt()),
    forceLogOut: () => dispatch(logOut()),
    updateProducts: (querySnapshot) => dispatch(updateProducts(querySnapshot)),
  };
}

function select(state) {
  return {
    user: state.user,
    productsData: _.sortBy(state.products.list, 'order'),
  };
}

module.exports = connect(select, actions)(HomeView);
