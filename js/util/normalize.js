import {Dimensions} from 'react-native';

const scale = Dimensions.get('window').width / 375;

function normalize(size) {
  return scale > 1 ? size : Math.round(scale * size);
}

module.exports = {
  normalize,
};