
import Fuse from 'fuse.js';
import _ from 'lodash';

function filterResults(products_list, newsletters_list, query) {
  // Merge everything together so only files are searched against
  let files = _.map(products_list, 'products_list');
  let product_files = [];
  files.forEach((file) => {
    product_files.push(_.valuesIn(file));
  });
  product_files = _.flatten(product_files);
  let all_files = [];
  product_files.forEach((file) => {
    let sales_files = _.valuesIn(file.sales_files);
    if (!_.isEmpty(sales_files)) {
      sales_files.map((f) => all_files.push({
        ...f,
        product_name: file.name,
      }));
    }
    let technical_files = _.valuesIn(file.technical_files);
    if (!_.isEmpty(technical_files)) {
      technical_files.map((f) => all_files.push({
        ...f,
        product_name: file.name,
      }));
    }
    let video_files = _.valuesIn(file.video_files);
    if (!_.isEmpty(video_files)) {
      video_files.map((f) => all_files.push({
        ...f,
        product_name: file.name,
      }));
    }
    let admin_files = _.valuesIn(file.admin_files);
    if (!_.isEmpty(admin_files)) {
      admin_files.map((f) => all_files.push({
        ...f,
        product_name: file.name,
      }));
    }
  });
  newsletters_list.forEach((file) => {
    all_files.push({
      ...file,
    });
  });

  // Search
  let options = {
    shouldSort: true,
    threshold: 0.6,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    minMatchCharLength: 1,
    keys: [
      "product_name",
      "file_name",
      "file_type",
      "product_number",
    ]
  };
  let fuse = new Fuse(all_files, options);
  let result = fuse.search(query);
  return result;
}

module.exports = {
  filterResults,
};