import _ from 'lodash';
import moment from 'moment';

function groupHistory(list) {
  list = _.orderBy(list, ['viewed_at'], ['desc']);
  const groupedHistory = _.groupBy(list, (h) => {
    return moment(h.viewed_at).format('MMMM D');
  });

  return groupedHistory;
}

module.exports = {
  groupHistory,
};