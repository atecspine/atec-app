import _ from 'lodash';
import Mailer from 'react-native-mail';

export const email = (files, callback) => {
  let body_content = _.map(files, (file) => {
    let full_file_url = file.full_file_url;
    if (full_file_url) {
      return `<a href="${file.full_file_url}">${file.file_name}</a><br/>`;
    } else {
      full_file_url = `https://firebasestorage.googleapis.com/v0/b/atec-spine-dev.appspot.com/o/${encodeURIComponent(file.file_url)}?alt=media`
      return `<p><a href="${file.full_file_url}">${file.file_name}</a></p>`
    }
  }).join(' ');
  Mailer.mail({
    subject: 'Here are some files',
    recipients: [''],
    ccRecipients: [''],
    bccRecipients: [''],
    body: `${body_content}`,
    isHTML: true,
  }, callback);
};
