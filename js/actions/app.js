function changeAppRoot(root) {
  return {
    type: 'CHANGED_APP_ROOT',
    root: root
  };
}

module.exports = {
  changeAppRoot,
};
