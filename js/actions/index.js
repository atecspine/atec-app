const appActions = require('./app');
const loginActions = require('./login');
const productActions = require('./products');
const searchActions = require('./search');
const historyActions = require('./history');
const favoritesActions = require('./favorites');
const foldersActions = require('./folders');
const folderMapActions = require('./folderMap');
const sharingActions = require('./sharing');
const downloadingOverlayActions = require('./downloadingOverlay');
const contactActions = require('./contact');
const announcementsActions = require('./announcements');
const newslettersActions = require('./newsletters');

module.exports = {
  ...appActions,
  ...loginActions,
  ...productActions,
  ...searchActions,
  ...historyActions,
  ...favoritesActions,
  ...foldersActions,
  ...folderMapActions,
  ...sharingActions,
  ...downloadingOverlayActions,
  ...contactActions,
  ...announcementsActions,
  ...newslettersActions,
};
