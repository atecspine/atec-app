import {
  ActionSheetIOS,
  Platform,
} from 'react-native';
import firebase from 'react-native-firebase';
import {changeAppRoot} from './app';


function loginWithEmail(email, password) {
  return (dispatch) => {
    return firebase.auth().signInAndRetrieveDataWithEmailAndPassword(email, password).then((user) => {
      dispatch(changeAppRoot('after-login'));
      dispatch({
        type: 'LOGGED_IN',
        user,
      });
    });
  };
}

function sendForgotPasswordEmail(email) {
  return (dispatch) => {
    return firebase.auth().sendPasswordResetEmail(email);
  };
}

function logOut() {
  return (dispatch) => {
    dispatch(changeAppRoot('login'));
    return dispatch({
      type: 'LOGGED_OUT'
    });
  };
}

function logOutWithPrompt() {
  return (dispatch, getState) => {
    if (Platform.OS === 'ios') {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          title: 'Log out from ATEC Spine?',
          options: ['Log out', 'Cancel'],
          destructiveButtonIndex: 0,
          cancelButtonIndex: 1,
        },
        (buttonIndex) => {
          if (buttonIndex === 0) {
            dispatch(logOut());
          }
        }
      );
    } else {
      Alert.alert(
        `Log Out`,
        'Log out from ATEC Spine?',
        [
          { text: 'Cancel' },
          { text: 'Log out', onPress: () => dispatch(logOut()) },
        ]
      );
    }
  };
}

module.exports = {
  loginWithEmail,
  logOut,
  logOutWithPrompt,
  sendForgotPasswordEmail,
};
