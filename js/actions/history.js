import moment from 'moment';

function addFileToHistory(file) {
  return {
    type: 'FILE_ADDED_TO_HISTORY',
    file: {
      ...file,
      viewed_at: moment().valueOf(),
    },
  };
}

module.exports = {
  addFileToHistory,
};