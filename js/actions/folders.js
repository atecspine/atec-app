
function addFolder(folder) {
  return {
    type: 'ADD_FOLDER',
    folder
  };
}

function removeFolders(folders) {
  return {
    type: 'REMOVE_FOLDERS',
    folders
  };
}

module.exports = {
  addFolder,
  removeFolders
};
