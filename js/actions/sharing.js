
import _ from 'lodash';

function addFileToShare(file) {
  return {
    type: 'ADD_FILE_TO_SHARING',
    file
  };
}

function removeFileFromShare(file) {
  return {
    type: 'REMOVE_FILE_FROM_SHARING',
    file
  };
}

function resetFileSharing() {
  return {
    type: 'RESET_FILES_TO_SHARE'
  };
}

function addFolderToShare(folder) {
  return {
    type: 'ADD_FOLDER_TO_SHARING',
    folder
  };
}

function removeFolderFromShare(folder) {
  return {
    type: 'REMOVE_FOLDER_FROM_SHARING',
    folder
  };
}

function resetFolderSharing() {
  return {
    type: 'RESET_FOLDERS_TO_SHARE'
  };
}

function toggleFileToShare(file) {
  return (dispatch, getState) => {
    let names = _.map(getState().sharing.selectedFiles, 'file_url');
    if (_.includes(names, file.file_url)) {
      dispatch(removeFileFromShare(file));
    } else {
      dispatch(addFileToShare(file));
    }
  };
}

function toggleFolderToShare(folder) {
  return (dispatch, getState) => {
    let names = getState().sharing.selectedFolders;
    if (_.includes(names, folder)) {
      dispatch(removeFolderFromShare(folder));
    } else {
      dispatch(addFolderToShare(folder));
    }
  };
}

module.exports = {
  toggleFileToShare,
  resetFileSharing,
  toggleFolderToShare,
  resetFolderSharing
};
