function addFilesToFolders(files, folders) {
  return {
    type: 'ADD_FILES_TO_FOLDERS',
    payload: { files, folders }
  }
}

function removeFilesFromFolder(files, folder) {
  return {
    type: 'REMOVE_FILES_FROM_FOLDER',
    payload: { files, folder }
  }
}

module.exports = {
  removeFilesFromFolder,
  addFilesToFolders
};
