import firebase from 'react-native-firebase';
import RNFetchBlob from 'react-native-fetch-blob';
import moment from 'moment';

function removeDownloadedFile(file) {
  let thumbnail_path = file.offline_thumbnail_path;
  if (thumbnail_path) {
    RNFetchBlob.fs.unlink(thumbnail_path).then(() => {});
  }
  let file_path = file.offline_path;
  if (file_path) {
    RNFetchBlob.fs.unlink(file_path).then(() => {});
  }
}

function fileStartedDownloading(file) {
  return {
    type: 'FILE_STARTED_DOWNLOADING',
    file,
  };
}

function fileFinishedDownloading(file) {
  return {
    type: 'FILE_FINISHED_DOWNLOADING',
    file,
  };
}

function updateFileDownloadProgress(bytesTransferred, totalBytes, file) {
  return {
    type: 'UPDATE_FILE_DOWNLOAD_PROGRESS',
    bytesTransferred,
    totalBytes,
    file,
  };
}

function downloadFile(file, dispatch) {
  let {file_thumbnail_url, file_url} = file;
  const file_thumbnail_ref = firebase.storage().ref(file_thumbnail_url);
  const file_ref = firebase.storage().ref(file_url);
  let thumbnail_path = `${firebase.storage.Native.CACHES_DIRECTORY_PATH}/${file_thumbnail_url}`;
  let file_path = `${firebase.storage.Native.CACHES_DIRECTORY_PATH}/${file_url}`;
  dispatch(fileStartedDownloading(file_url));
  file_thumbnail_ref.getDownloadURL().then((full_thumbnail_url) => {
    file_thumbnail_ref.downloadFile(thumbnail_path)
    .then((response) => {
      file_ref.getDownloadURL().then((full_file_url) => {
        const unsubscribe = file_ref.downloadFile(file_path).on(
          firebase.storage.TaskEvent.STATE_CHANGED,
          (snapshot) => {
            dispatch(updateFileDownloadProgress(snapshot.bytesTransferred, snapshot.totalBytes, file_url));
          },
          (error) => {
            unsubscribe();
            console.log(error);
          }, (uploadedFile) => {
            //Success
            unsubscribe();
            dispatch(fileFinishedDownloading(file_url));
            dispatch(fileSavedForOffline({
              ...file,
              full_thumbnail_url: full_thumbnail_url,
              full_file_url: full_file_url,
              offline_thumbnail_path: thumbnail_path,
              offline_path: file_path,
              added_on: moment().valueOf(),
            }));
          }
        );
      });
    });
  });
}

function addFavoriteFile(file) {
  return {
    type: 'ADD_FAVORITE_FILE',
    file,
    added_on: moment().valueOf(),
  };
}

function removeFavoriteFile(file) {
  return {
    type: 'REMOVE_FAVORITE_FILE',
    file,
  };
}

function fileSavedForOffline(file) {
  return {
    type: 'FILE_SAVED_FOR_OFFLINE',
    file,
  };
}

function toggleFavoriteFile(file) {
  return (dispatch, getState) => {
    let favorites = getState().favorites.list;
    if (favorites.some(f => f.file_url === file.file_url)) {
      dispatch(removeFavoriteFile(file));
      removeDownloadedFile(file);
    } else {
      dispatch(addFavoriteFile(file));
      downloadFile(file, dispatch);
    }
  };
}

module.exports = {
  toggleFavoriteFile,
};