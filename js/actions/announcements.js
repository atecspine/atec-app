

function updateAnnouncements(querySnapshot) {
  return (dispatch) => {
    let announcements_list = [];
    querySnapshot.forEach(doc => {
      announcements_list.push(doc.data());
    });
    dispatch({
      type: 'ANNOUNCEMENTS_UPDATED',
      announcements: announcements_list,
    });
  };
}

module.exports = {
  updateAnnouncements,
};