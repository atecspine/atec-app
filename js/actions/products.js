

function updateProducts(querySnapshot) {
  return (dispatch) => {
    let products_list = [];
    querySnapshot.forEach(doc => {
      products_list.push(doc.data());
    });
    dispatch({
      type: 'PRODUCTS_UPDATED',
      products: products_list,
    });
  };
}

module.exports = {
  updateProducts,
};