function hideDownloadingOverlay() {
  return {
    type: 'HIDE_DOWNLOADING_OVERLAY',
  };
}

module.exports = {
  hideDownloadingOverlay,
};
