

function updateContact(querySnapshot) {
  return (dispatch) => {
    let contact_list = [];
    querySnapshot.forEach(doc => {
      contact_list.push(doc.data());
    });
    dispatch({
      type: 'CONTACT_UPDATED',
      contacts: contact_list,
    });
  };
}

module.exports = {
  updateContact,
};