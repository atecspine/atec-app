

function updateNewsletters(querySnapshot) {
  return (dispatch) => {
    let newsletters_list = [];
    querySnapshot.forEach(doc => {
      newsletters_list.push(doc.data());
    });
    dispatch({
      type: 'NEWSLETTERS_UPDATED',
      newsletters: newsletters_list,
    });
  };
}

module.exports = {
  updateNewsletters,
};