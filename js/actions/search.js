function updateSearchQuery(text) {
  return {
    type: 'SEARCH_QUERY_UPDATED',
    query: text,
  };
}

module.exports = {
  updateSearchQuery,
};
