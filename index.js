
import {Provider} from 'react-redux';
import configureStore from './js/store/configureStore';
import {Platform} from 'react-native';
import {persistStore} from 'redux-persist';
import {Navigation} from 'react-native-navigation';
import {registerScreens} from './js/screens';
import AppColors from './js/common/AppColors';

function setup() {
  console.disableYellowBox = true;

  const store = configureStore();

  class Root {
    constructor() {
      persistStore(store, null, () => {
        registerScreens(store, Provider);

        this.onStoreUpdate();
        store.subscribe(this.onStoreUpdate.bind(this));
      });
    }

    onStoreUpdate() {
      const {root} = store.getState().app;
      // handle a root change
      if (this.currentRoot !== root) {
        this.currentRoot = root;
        this.startApp(root);
      }
    }

    startApp(root) {
      switch (root) {
        case 'login':
          Navigation.startSingleScreenApp({
            screen: {
              screen: 'atec.LoginScreen',
              title: 'ATEC Spine',
              titleImage: require('./js/common/img/header-logo.png')
            },
            passProps: {},
            animationType: 'none',
            appStyle: {
              navBarTextFontFamily: 'Soleto',
              navBarTitleFontFamily: 'Soleto',
              navBarSubtitleFontFamily: 'Soleto',
              navBarButtonFontFamily: 'Soleto',
              navBarTitleFontSize: 18,
              navBarTitleFontWeight: '500',
              navBarBackgroundColor: AppColors.darkBlue,
              screenBackgroundColor: AppColors.white,
              navBarTextColor: AppColors.white,
              navBarButtonColor: AppColors.brightGreen,
              statusBarTextColorScheme: 'light',
              navBarSubtitleColor: AppColors.white,
              topBarBorderColor: AppColors.brightGreen,
              navBarNoBorder: true
            }
          });
          return;
        case 'after-login':
          if (Platform.OS === 'android') {
            Navigation.startSingleScreenApp({
              screen: {
                screen: 'atec.HomeView',
                title: 'Products',
                titleImage: require('./js/common/img/header-logo.png')
              },
              drawer: {
                left: {
                  screen: 'atec.DrawerView'
                }
              },
              passProps: {},
              animationType: 'fade'
            });
          } else if (Platform.OS === 'ios') {
            Navigation.startTabBasedApp({
              tabs: [
                {
                  label: 'Home',
                  screen: 'atec.HomeView',
                  icon: require('./js/tabs/products/img/home-icon.png'),
                  selectedIcon: require('./js/tabs/products/img/home-icon.png'),
                  title: 'Home',
                  titleImage: require('./js/common/img/header-logo.png')
                },
                {
                  label: 'Contact',
                  screen: 'atec.ContactView',
                  icon: require('./js/tabs/contact/img/contact-icon.png'),
                  selectedIcon: require('./js/tabs/contact/img/contact-icon.png'),
                  title: 'Contact',
                  titleImage: require('./js/common/img/header-logo.png')
                },
                {
                  label: 'Frontline',
                  screen: 'atec.NewslettersView',
                  icon: require('./js/tabs/newsletters/img/frontline-icon.png'),
                  selectedIcon: require('./js/tabs/newsletters/img/frontline-icon.png'),
                  title: 'Frontline',
                  titleImage: require('./js/common/img/header-logo.png')
                },
                {
                  label: 'History',
                  screen: 'atec.HistoryView',
                  icon: require('./js/tabs/history/img/history-icon.png'),
                  selectedIcon: require('./js/tabs/history/img/history-icon.png'),
                  title: 'History',
                  titleImage: require('./js/common/img/header-logo.png')
                },
                {
                  label: 'Favorites',
                  screen: 'atec.FavoritesView',
                  icon: require('./js/tabs/favorites/img/favorites-icon.png'),
                  selectedIcon: require('./js/tabs/favorites/img/favorites-icon.png'),
                  title: 'Favorites',
                  titleImage: require('./js/common/img/header-logo.png')
                }
              ],
              tabsStyle: {
                tabBarButtonColor: AppColors.white,
                tabBarSelectedButtonColor: AppColors.actionText,
                tabBarBackgroundColor: AppColors.tabBarBackground,
                tabBarTranslucent: false,
                tabBarSelectedTextColor: AppColors.actionText,
                tabBarTextColor: AppColors.white,
                tabBarTextFontFamily: 'Soleto',
                tabBarTextFontSize: 12
              },
              appStyle: {
                navBarTextFontFamily: 'Soleto',
                navBarTitleFontFamily: 'Soleto',
                navBarSubtitleFontFamily: 'Soleto',
                navBarButtonFontFamily: 'Soleto',
                navBarButtonFontSize: 16,
                navBarTitleFontSize: 18,
                navBarTitleFontWeight: '500',
                navBarBackgroundColor: AppColors.darkBlue,
                screenBackgroundColor: AppColors.white,
                navBarTextColor: AppColors.white,
                navBarButtonColor: AppColors.brightGreen,
                topBarBorderColor: AppColors.brightGreen,
                statusBarTextColorScheme: 'light',
                navBarSubtitleColor: AppColors.white,
                navBarNoBorder: true
              },
              passProps: {},
              animationType: 'fade'
            });
          }
          return;
        default:
          return;
      }
    }
	}

  return new Root();
}

setup();
